A : Z
B : X
X :
Y :

L : L-Shift
R : A
ZL : Q
ZR : E

start : Enter
select : Esc

↑ : ↑
↓ : ↓
→ : →
← : ←

LS ↑ : I
LS ↓ : K
LS → : L
LS ← : J

RS ↑ : Num8
RS ↓ : Num5
RS → : Num6
RS ← : Num4