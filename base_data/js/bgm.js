function bgm(str) {
    BGM.pause();
    switch (str) {
        case "title":
            BGM.src = "bgm/title/title.mp3";
            break;

        case "select":
            BGM.src = "bgm/select/0001.mp3";
            break;

        case "config":
            BGM.src = "bgm/select/0001.mp3";
            break;

        case "編成":
            BGM.src = "bgm/select/0001.mp3";
            break;

        case "battle":
            BGM.src = "bgm/battle/0001.mp3";
            break;

        default:
            str = "停止";
            break;
    }
    if (str != "停止") {
        BGM.loop = "true";
        BGM.play();
    }
}

function se(str) {
    SE.pause();
    var data = ["se/select.wav", "se/back.wav"];
    if (str == "決定") SE.src = data[0];
    else if (str == "lv_up") SE.src = "bgm/battle/lv_up.mp3";
    else SE.src = data[1];
    SE.play();
}