function 戦闘開始文字(num) {
    var a = document.getElementsByClassName("バトルスタート文字");
    var b = document.getElementById("戦闘開始文字");
    var c = document.getElementById("battle2");
    c.style.display = "inline-block";
    b.style.display = "inline-block";
    a[num].style.display = "inline-block";
    if (num < 13) {
        setTimeout(function() {
            戦闘開始文字(num + 1);
        }, 62.5);
    }
    if (num == 12) {
        setTimeout(function() {
            c.style.display = "none";
            var d = 0;
            while (d < a.length) {
                a[d].style.display = "none";
                d++;
            }
            b.style.display = "none";
            user = true;
        }, 1000);
    }
}

function 戦闘画面へ() {
    var a = Number(document.getElementById("skip").innerHTML);
    if (a == 1) {
        if (難易度 == "EXTRA") 敵配置[5][2] = 4;
        else 敵配置[5][2] = 5;
        現在画面 = "battle2";
        user = false;
        var cnt = 編成中のキャラ[0] + 編成中のキャラ[1] + 編成中のキャラ[2];
        if (cnt > 0) {
            全非表示();
            document.getElementById("btm").style.display = "none";
            document.getElementById("play_char").style.backgroundImage = "";
            var a = 0;
            while (a < 3) {
                document.getElementsByClassName("戦闘画面_敵")[a].style.display = "block";
                document.getElementsByClassName("戦闘画面_敵")[a].style.display = "none";
                document.getElementsByClassName("戦闘画面_プレイヤー")[a].style.display = "none";
                document.getElementsByClassName("戦闘画面_キャラ画像")[a + 3].style.backgroundImage = "url(" + キャラ[0].img + ")";
                document.getElementsByClassName("戦闘画面_キャラ画像")[a].style.backgroundImage = "url(" + キャラ[0].img + ")";
                if (編成中のキャラ[a] > 0) document.getElementsByClassName("戦闘画面_プレイヤー")[a].style.display = "block";
                if (敵配置[選択ステージ][a + 1] > 0) document.getElementsByClassName("戦闘画面_敵")[a].style.display = "block";

                var b = 1;
                while (b < キャラ.length) {
                    if (編成中のキャラ[a] == キャラ[b].num) {
                        //プレイヤー
                        if (言語 == "jp") document.getElementsByClassName("戦闘画面_プレイヤー名")[a].innerHTML = キャラ[b].名前;
                        else document.getElementsByClassName("戦闘画面_プレイヤー名")[a].innerHTML = キャラ[b].name;
                        document.getElementsByClassName("戦闘画面_プレイヤー_HP")[a].innerHTML = キャラ[b].HP;
                        document.getElementsByClassName("戦闘画面_プレイヤー_MP")[a].innerHTML = キャラ[b].MP;
                        キャラ_戦闘[a].名前 = キャラ[b].名前;
                        キャラ_戦闘[a + 3].名前 = キャラ[b].名前;
                        キャラ_戦闘[a].HP = キャラ[b].HP;
                        キャラ_戦闘[a + 3].HP = キャラ[b].HP;
                        キャラ_戦闘[a].MP = キャラ[b].MP;
                        キャラ_戦闘[a + 3].MP = キャラ[b].MP;
                        キャラ_戦闘[a].Lv = キャラ[b].Lv;
                        キャラ_戦闘[a + 3].Lv = キャラ[b].Lv;
                        キャラ_戦闘[a].ATK = キャラ[b].ATK;
                        キャラ_戦闘[a + 3].ATK = キャラ[b].ATK;
                        キャラ_戦闘[a].MAT = キャラ[b].MAT;
                        キャラ_戦闘[a + 3].MAT = キャラ[b].MAT;
                        キャラ_戦闘[a].SPD = キャラ[b].SPD;
                        キャラ_戦闘[a + 3].SPD = キャラ[b].SPD;
                        戦闘_レベル[0][a + 3] = キャラ[b].Lv;
                        戦闘_レベル[1][a + 3] = キャラ[b].名前;
                        document.getElementsByClassName("戦闘画面_キャラ画像")[a + 3].style.backgroundImage = "url(" + キャラ[b].img + ")";
                        break;
                    }
                    b++;
                }
                敵情報初期設定(a);

                if (選択ステージ == 5) {
                    var z = document.getElementsByClassName("戦闘画面_キャラ画像");
                    z[0].style.width = "0vw";
                    z[2].style.width = "0vw";
                    if (難易度 != "EXTRA") z[1].style.backgroundImage = "img/char/mob/0000005.png";
                    else z[1].style.backgroundImage = "img/char/mob/0000006.png";
                    z[1].style.backgroundSize = "contain";
                    z[1].style.width = "100%";
                } else {
                    var z = document.getElementsByClassName("戦闘画面_キャラ画像");
                    z[0].style.width = "33%";
                    z[1].style.width = "33%";
                    z[2].style.width = "33%";
                }

                var name = [
                    [],
                    []
                ];
                var name_a = 0;
                while (name_a < キャラ.length) {
                    name[0][name_a] = キャラ[name_a].名前;
                    name[1][name_a] = キャラ[name_a].num;
                    name_a++;
                }
                編成中のキャラ_名前[a] = name[0][name[1].indexOf(編成中のキャラ[a])];
                a++;
            }

            var a = 0;
            var b = 0;
            while (a < キャラSPD.length) {
                b = 0;
                while (b < キャラSPD[a].length) {
                    if (a % 2 == 0) キャラSPD[a][b] = -1;
                    else キャラSPD[a][b] = null;
                    b++;
                }
                a++;
            }
            キャラSPD設定();
            キャラSPD[6] = [...キャラSPD[4]];
            キャラSPD[7] = [...キャラSPD[5]];

            var a = 0;
            var b = [];
            var c = [];
            while (a < キャラ.length) {
                b[a] = キャラ[a].名前;
                c[a] = キャラ[a].img;
                a++;
            }
            var d = b.indexOf(キャラSPD[5][0]);
            if (d >= 0) document.getElementById("play_char").style.backgroundImage = "url(" + c[d] + ")";

            setTimeout(function() {
                document.getElementById("battle").style.display = "block";
                bgm("battle");
                戦闘開始文字(0);
                setTimeout(function() {
                    敵選択(1);
                    現在画面 = "battle";
                    戦闘開始();
                }, 2000);
            }, 250);
        } else {
            alert("1人以上キャラを選択してください");
        }
        var a = document.getElementsByClassName("戦闘画面_敵名")[0].innerHTML;
    } else {
        se("戻る");
    }
}

const 配列内重複要素検索 = ([...array]) => {
    return array.filter((value, index, self) => self.indexOf(value) === index && self.lastIndexOf(value) !== index);
}

function キャラSPD設定() {
    //キャラSPD[0] : 画面表示のまま各キャラのSPD配置
    //キャラSPD[1] : キャラSPD[0]のキャラ名
    //キャラSPD[2] : SPD降順に並べ替え
    //キャラSPD[3] : キャラSPD[2]のキャラ名
    //キャラSPD[4] : SPDが早いやつが値が小さくなるように変換
    //キャラSPD[5] : キャラSPD[3]のキャラ名
    //キャラSPD[6] : キャラSPD[4]のバックアップ
    //キャラSPD[7] : キャラSPD[5]のバックアップ

    if (Number(敵配置[選択ステージ][1]) > 0) キャラSPD[0][0] = 敵キャラ[Number(敵配置[選択ステージ][1])].SPD;
    if (Number(敵配置[選択ステージ][2]) > 0) キャラSPD[0][1] = 敵キャラ[Number(敵配置[選択ステージ][2])].SPD;
    if (Number(敵配置[選択ステージ][3]) > 0) キャラSPD[0][2] = 敵キャラ[Number(敵配置[選択ステージ][3])].SPD;
    if (編成中のキャラ[0] > 0) キャラSPD[0][3] = キャラ[編成中のキャラ[0]].SPD;
    if (編成中のキャラ[1] > 0) キャラSPD[0][4] = キャラ[編成中のキャラ[1]].SPD;
    if (編成中のキャラ[2] > 0) キャラSPD[0][5] = キャラ[編成中のキャラ[2]].SPD;

    if (Number(敵配置[選択ステージ][1]) > 0) キャラSPD[1][0] = 敵キャラ[Number(敵配置[選択ステージ][1])].名前;
    if (Number(敵配置[選択ステージ][2]) > 0) キャラSPD[1][1] = 敵キャラ[Number(敵配置[選択ステージ][2])].名前;
    if (Number(敵配置[選択ステージ][3]) > 0) キャラSPD[1][2] = 敵キャラ[Number(敵配置[選択ステージ][3])].名前;
    if (編成中のキャラ[0] > 0) キャラSPD[1][3] = キャラ[編成中のキャラ[0]].名前;
    if (編成中のキャラ[1] > 0) キャラSPD[1][4] = キャラ[編成中のキャラ[1]].名前;
    if (編成中のキャラ[2] > 0) キャラSPD[1][5] = キャラ[編成中のキャラ[2]].名前;

    //SPDによる重複バグ対策
    var aa = 0;
    while (aa == 0) {
        var a = [];
        var b = 0;
        var c = 0;
        while (b < キャラSPD[0].length) {
            if (キャラSPD[0][b] > -1) {
                a[c] = Number(キャラSPD[0][b]);
                c++;
            }
            b++;
        }
        var d = 配列内重複要素検索(a);
        if (d.length == 0) break;
        if (d.length > 0) {
            var a = 0;
            var b = 0;
            var c = 0;
            while (a < キャラSPD[0].length) {
                c = 0;
                while (c < d.length) {
                    if (キャラSPD[0][a] > -1 && キャラSPD[0][a] == d[c]) {
                        if (b % 2 == 0) キャラSPD[0][a] += run(5, 1);
                        else キャラSPD[0][a] -= run(5, 1);
                        b++;
                    }
                    c++;
                }
                a++;
            }
        }
    }
    //SPDによる重複バグ対策　ここまで

    キャラSPD[2] = [...ソート(キャラSPD[0], "降順")];

    var a = 0;
    while (a < キャラSPD[0].length) {
        var b = キャラSPD[1][キャラSPD[0].indexOf(キャラSPD[2][a])];
        if (b != "") キャラSPD[3][a] = b;
        a++;
    }

    a = 0;
    var b = Math.max(...キャラSPD[0]);
    while (a < キャラSPD[0].length) {
        if (キャラSPD[2][a] > 0) キャラSPD[4][a] = ((b - キャラSPD[2][a]) + (b * 2));
        a++;
    }
    キャラSPD[5] = [...キャラSPD[3]];

    行動順表示();
}

function 行動順表示() {
    var a = document.getElementsByClassName("行動順");
    var b = 0;
    var c = [];
    var d = [];
    var e = [];
    var ccc = 0;
    while (b < キャラ.length) {
        c[b] = キャラ[b].名前;
        b++;
    }
    b = 0;
    while (b < 敵キャラ.length) {
        e[b] = 敵キャラ[b].名前;
        b++;
    }
    b = 0;
    var bb = 0;
    while (b < (a.length - 1)) {
        if (キャラSPD[5][b] != null) {
            if (c.indexOf(キャラSPD[5][b]) >= 0) d[b - bb] = キャラ[c.indexOf(キャラSPD[5][b])].img;
            else d[b - bb] = 敵キャラ[e.indexOf(キャラSPD[5][b])].img;
        } else {
            bb++;
        }
        b++;
    }
    if (bb > 0) {
        var aa = (a.length - 1) - bb;
        var bb = 0;
        while (aa < (a.length - 1)) {
            bb++;
            aa++;
        }
        ccc = bb;
    }
    bb = Number(a.length) - 1;
    var ee = [...d.reverse()];
    var aa = 0;
    while ((bb - ccc) >= 0) {
        a[aa + ccc].style.backgroundImage = "url(" + ee[aa] + ")";
        aa++
        bb--;
    }
    if (ccc > 0) {
        var aa = 0;
        while (aa < ccc) {
            a[aa].style.backgroundImage = "url()";
            aa++;
        }
    }
}

function 敵情報初期設定(a) {
    //敵
    var d = 1;
    if (難易度 == "EASY") d = 0.5;
    if (難易度 == "HARD") d = 1.5;
    if (難易度 == "LUNATIC") d = 2;
    if (難易度 == "EXTRA") d = 2.5;
    var b = Number(敵配置[選択ステージ][a + 1]);
    敵キャラ_ローカル[b].Lv = Math.floor(Number(敵配置[選択ステージ][a + 4]) * d);
    戦闘_レベル[0][a] = 敵キャラ_ローカル[b].Lv;
    戦闘_レベル[1][a] = 敵キャラ_ローカル[b].名前;
    if (敵キャラ_ローカル[b].Lv < 1) 敵キャラ_ローカル[b].Lv = 1;
    var c = Number((1 + ((敵キャラ_ローカル[b].Lv - 1) / 10)));
    敵キャラ_ローカル[b].HP *= c;
    敵キャラ_ローカル[b].HP = Math.floor(敵キャラ_ローカル[b].HP);
    敵キャラ_ローカル[b].MP *= c;
    敵キャラ_ローカル[b].MP = Math.floor(敵キャラ_ローカル[b].MP);
    敵キャラ_ローカル[b].ATK *= c;
    敵キャラ_ローカル[b].ATK = Math.floor(敵キャラ_ローカル[b].ATK);
    敵キャラ_ローカル[b].MAT *= c;
    敵キャラ_ローカル[b].MAT = Math.floor(敵キャラ_ローカル[b].MAT);
    敵キャラ_ローカル[b].DEF *= c;
    敵キャラ_ローカル[b].DEF = Math.floor(敵キャラ_ローカル[b].DEF);
    敵キャラ_ローカル[b].MDF *= c;
    敵キャラ_ローカル[b].MDF = Math.floor(敵キャラ_ローカル[b].MDF);
    敵キャラ_ローカル[b].SPD *= c;
    敵キャラ_ローカル[b].SPD = Math.floor(敵キャラ_ローカル[b].SPD);
    敵キャラ_ローカル[b].LUK *= c;
    敵キャラ_ローカル[b].LUK = Math.floor(敵キャラ_ローカル[b].LUK);
    戦闘_敵選択_名前[a] = 敵キャラ_ローカル[b].名前;
    if (言語 == "jp") document.getElementsByClassName("戦闘画面_敵名")[a].innerHTML = 敵キャラ_ローカル[b].名前 + "<span style='font-size: 50%'>(Lv." + 敵キャラ_ローカル[b].Lv + ")</span>";
    else document.getElementsByClassName("戦闘画面_敵名")[a].innerHTML = 敵キャラ_ローカル[b].name + "<span style='font-size: 50%'>(Lv." + 敵キャラ_ローカル[b].Lv + ")</span>";
    document.getElementsByClassName("戦闘画面_敵_HP")[a].innerHTML = 敵キャラ_ローカル[b].HP;
    document.getElementsByClassName("戦闘画面_敵_MP")[a].innerHTML = 敵キャラ_ローカル[b].MP;
    document.getElementsByClassName("戦闘画面_キャラ画像")[a].style.backgroundImage = "url(" + 敵キャラ_ローカル[b].img + ")";
    敵情報[0][a] = 敵キャラ_ローカル[b].HP;
    敵情報[1][a] = 敵キャラ_ローカル[b].MP;
}

function 戦闘開始() {
    var a = 0;
    while (a < 3) {
        敵情報初期設定(a);
        a++;
    }
    setTimeout(function() {
        戦闘開始時処理_配列入れ替え();
        戦闘開始時処理_ログ表示();
        if (現在画面 == "battle") 戦闘中_ステータス表示();
    }, 10);

    setTimeout(function() {
        動作コマンド();
        プレイヤー判定();
    }, 1000);
}

function 動作コマンド() {
    var player_name = [キャラ[編成中のキャラ[0]].名前, キャラ[編成中のキャラ[1]].名前, キャラ[編成中のキャラ[2]].名前];
    if (キャラSPD[5][0] != player_name[0] && キャラSPD[5][0] != player_name[1] && キャラSPD[5][0] != player_name[0]) {
        //敵キャラの動作だった場合の、敵の動作
    }
    if (現在画面 == "battle") setTimeout(動作コマンド, 1000);
}

function 戦闘中_ステータス表示() {
    if (現在画面 == "battle") {
        var 開発用戦闘スキップ = false;
        var a = 1;
        var LvUp前キャラ_Lv = [];
        LvUp前キャラ_Lv[0] = 1;
        while (a < キャラ.length) {
            LvUp前キャラ_Lv[a] = キャラ[a].Lv;
            a++;
        }
        //敵サイド
        var a = 0;
        var h = 0;
        while (a < 3) {
            var b = Number(敵配置[選択ステージ][a + 1]);
            var f = 敵キャラ[b].名前;
            var 敵_MAX_HP = 敵情報[0][a];
            var 敵_MAX_MP = 敵情報[1][a];
            var HP = 敵キャラ_ローカル[b].HP;
            var MP = 敵キャラ_ローカル[b].MP;
            if (HP < 0) 敵キャラ_ローカル[b].HP = 0;
            h += 敵キャラ_ローカル[b].HP;
            if (開発用戦闘スキップ == true) {
                if (HP >= 0) 敵キャラ_ローカル[(敵配置[選択ステージ][a + 1])].HP -= (10 ** 100);
                if (MP >= 0) 敵キャラ_ローカル[(敵配置[選択ステージ][a + 1])].MP--;
            }

            if (敵キャラ_ローカル[(敵配置[選択ステージ][a + 1])].HP >= 0) {
                if (言語 == "jp") document.getElementsByClassName("戦闘画面_敵名")[a].innerHTML = 敵キャラ_ローカル[b].名前 + "<span style='font-size: 50%'>(Lv." + 敵キャラ_ローカル[b].Lv + ")</span>";
                else document.getElementsByClassName("戦闘画面_敵名")[a].innerHTML = 敵キャラ_ローカル[b].name + "<span style='font-size: 50%'>(Lv." + 敵キャラ_ローカル[b].Lv + ")</span>";
                if (HP > 0) document.getElementsByClassName("戦闘画面_敵_HP")[a].innerHTML = HP;
                else document.getElementsByClassName("戦闘画面_敵_HP")[a].innerHTML = 0;
                if (MP > 0) document.getElementsByClassName("戦闘画面_敵_MP")[a].innerHTML = MP;
                else document.getElementsByClassName("戦闘画面_敵_MP")[a].innerHTML = 0;
                document.getElementsByClassName("HPバー")[a].optimum = 敵_MAX_HP;
                document.getElementsByClassName("MPバー")[a].optimum = 敵_MAX_MP;
                document.getElementsByClassName("HPバー")[a].low = (敵_MAX_HP * 0.2);
                document.getElementsByClassName("HPバー")[a].high = (敵_MAX_HP * 0.8);
                document.getElementsByClassName("MPバー")[a].low = (敵_MAX_MP * 0.2);
                document.getElementsByClassName("MPバー")[a].high = (敵_MAX_MP * 0.8);
                document.getElementsByClassName("HPバー")[a].max = 敵_MAX_HP;
                document.getElementsByClassName("MPバー")[a].max = 敵_MAX_MP;
                document.getElementsByClassName("HPバー")[a].value = HP;
                document.getElementsByClassName("MPバー")[a].value = MP;
                if (HP < 1) {
                    var g = キャラSPD[5].indexOf(f);
                    キャラSPD[4][g] = -1;
                    キャラSPD[5][g] = null;
                }
            }
            if (敵キャラ_ローカル[(敵配置[選択ステージ][a + 1])].HP == 0) {
                document.getElementsByClassName("戦闘画面_敵")[a].style.display = "none";
                document.getElementsByClassName("戦闘画面_キャラ画像")[a].style.backgroundImage = "url()";
            }
            if (キャラ_戦闘[a].HP <= 0) {
                var names = document.getElementsByClassName("戦闘画面_プレイヤー名")[a].innerHTML;
                document.getElementsByClassName("戦闘画面_プレイヤー")[a].style.display = "none";
                document.getElementsByClassName("戦闘画面_キャラ画像")[a + 3].style.backgroundImage = "url()";
                var g = キャラSPD[5].indexOf(キャラ_戦闘[a].名前);
                キャラSPD[4][g] = -1;
                キャラSPD[5][g] = null;
            }

            if (キャラ_戦闘[a].HP >= 0) {
                var HP = キャラ_戦闘[a].HP;
                var MP = キャラ_戦闘[a].MP;
                var HP_base = キャラ_戦闘[a + 3].HP;
                var MP_base = キャラ_戦闘[a + 3].MP;
                if (HP < 0) HP = 0;
                if (MP < 0) MP = 0;
                var HPバー = (HP / HP_base) * 100;
                var MPバー = (MP / MP_base) * 100;
                var HP_color = "";
                var MP_color = "";
                //各種表示関連
                if (HPバー > 100) HPバー = 100;
                if (MPバー > 100) MPバー = 100;
                if (HP > 0) document.getElementsByClassName("戦闘画面_プレイヤー_HP")[a].innerHTML = HP;
                else document.getElementsByClassName("戦闘画面_プレイヤー_HP")[a].innerHTML = 0;
                if (MP > 0) document.getElementsByClassName("戦闘画面_プレイヤー_MP")[a].innerHTML = MP;
                else document.getElementsByClassName("戦闘画面_プレイヤー_MP")[a].innerHTML = 0;
                if (HP > HP_base) document.getElementsByClassName("HPバー")[a + 3].max = HP;
                else document.getElementsByClassName("HPバー")[a + 3].max = HP_base;
                if (MP > MP_base) document.getElementsByClassName("MPバー")[a + 3].max = MP;
                else document.getElementsByClassName("MPバー")[a + 3].max = MP_base;

                document.getElementsByClassName("HPバー")[a + 3].low = (HP_base * 0.2);
                document.getElementsByClassName("HPバー")[a + 3].high = (HP_base * 0.5);
                document.getElementsByClassName("MPバー")[a + 3].low = (MP_base * 0.2);
                document.getElementsByClassName("MPバー")[a + 3].high = (MP_base * 0.5);
                document.getElementsByClassName("HPバー")[a + 3].optimum = HP_base;
                document.getElementsByClassName("MPバー")[a + 3].optimum = MP_base;
                document.getElementsByClassName("HPバー")[a + 3].value = HP;
                document.getElementsByClassName("MPバー")[a + 3].value = MP;
                if ((HP_base - HP) < 0) {
                    var 超過分HPバー = document.getElementsByClassName("超過分HPバー");
                    超過分HPバー[a].style.display = "block";
                    if (超過分[0][a] < (HP - HP_base)) {
                        超過分[0][a] = HP - HP_base;
                        超過分HPバー[a].max = 超過分[0][a];
                        超過分HPバー[a].optimum = HP - HP_base;
                        超過分HPバー[a].high = (HP - HP_base) * 0.5;
                        超過分HPバー[a].low = (HP - HP_base) * 0.2;
                    }
                    超過分HPバー[a].value = HP - HP_base;
                    document.getElementsByClassName("戦闘画面_プレイヤー_HP")[a].innerHTML = HP_base + "<br><span style='color: limegreen'>+" + (HP - HP_base) + "</span>";
                } else {
                    document.getElementsByClassName("超過分HPバー")[a].style.display = "none";
                    document.getElementsByClassName("戦闘画面_プレイヤー_HP")[a].innerHTML = HP;
                }
                if ((MP_base - MP) < 0) {
                    var 超過分MPバー = document.getElementsByClassName("超過分MPバー");
                    超過分MPバー[a].style.display = "block";
                    if (超過分[1][a] < MP) 超過分[1][a] = MP - MP_base;
                    超過分MPバー[a].max = 超過分[1][a];
                    超過分MPバー[a].optimum = MP - MP_base;
                    超過分MPバー[a].value = MP - MP_base;
                    document.getElementsByClassName("戦闘画面_プレイヤー_MP")[a].innerHTML = MP_base + "<br><span style='color: limegreen'>+" + (MP - MP_base) + "</span>";
                } else {
                    document.getElementsByClassName("超過分MPバー")[a].style.display = "none";
                    document.getElementsByClassName("戦闘画面_プレイヤー_MP")[a].innerHTML = MP;
                }
            }

            if (敵キャラ_ローカル[b].HP < 1) document.getElementsByClassName("戦闘画面_敵_HP")[a].innerHTML = 0;
            if (敵キャラ_ローカル[b].MP < 1) document.getElementsByClassName("戦闘画面_敵_MP")[a].innerHTML = MP;
            a++;
        }
        //敵HP全損
        if (h < 1) {
            setTimeout(function() {
                var level = [];
                level[0] = 200;
                level[1] = 0;
                var a = 2;
                while (a <= 100) {
                    level[a] = Math.floor(level[0] * a ** 3);
                    a++;
                }
                var a = 0;
                while (a < 3) {
                    if (編成中のキャラ[a] == 0) document.getElementsByClassName("勝利_キャラ")[a].style.opacity = 0;
                    else document.getElementsByClassName("勝利_キャラ")[a].style.opacity = 1;
                    a++;
                }
                現在画面 = "victory";
                コイン += (run(1000, 1) * 選択ステージ);
                var a = 0;
                var d = 0;
                var e = 100;
                var f = 0;
                while (a < 3) {
                    var b = Number(敵配置[選択ステージ][a + 1]);
                    var c = Number((1 + ((敵キャラ_ローカル[b].Lv - 1) / 10)));
                    d += Math.floor(敵キャラ[b].HP * c);
                    if (キャラ[編成中のキャラ[a]].Lv < e) e = キャラ[編成中のキャラ[a]].Lv;
                    if (キャラ[編成中のキャラ[a]].Lv > f) f = キャラ[編成中のキャラ[a]].Lv;
                    a++;
                }
                if (選択ステージ != 5) d /= 3;
                else {
                    d = (d / (e * 500)) * f * 7.5;
                }
                a = 0;
                while (a < 3) {
                    var e = キャラ[編成中のキャラ[a]].EXP;
                    var f = Math.floor(Math.floor(d) ** (1 + (user_lank / 100)));
                    if (f > 999999999) f = 999999999;
                    else if (選択ステージ != 5) f = Math.floor(f * 7.5);
                    /*
                    if (user_lank < 15 * 1.5) f = Math.floor(f * 10 * 2);
                    else if (user_lank < 20 * 1.5) f = Math.floor(f * 7.5 * 2);
                    else if (user_lank < 25 * 1.5) f = Math.floor(f * 5 * 2);
                    else if (user_lank < 30 * 1.5) f = Math.floor(f * 2.5 * 2);
                    */
                    var g = e + f;
                    var i = "";
                    var j = キャラ[編成中のキャラ[a]].Lv;
                    var k = f;
                    if (g > 999999999) g = 999999999;
                    if (j < 10) {
                        i = "<span style='color: red;'>&nbsp;<span style='font-size: 75%'>×2&nbsp;(BONUS)</span></span>";
                        g = Math.floor(g * 2);
                    } else if (j < 15) {
                        i = "<span style='color: red;'>&nbsp;<span style='font-size: 75%'>×1.5&nbsp;(BONUS)</span></span>";
                        g = Math.floor(g * 1.5);
                    } else if (j < 25) {
                        i = "<span style='color: red;'>&nbsp;<span style='font-size: 75%'>×1.25&nbsp;(BONUS)</span></span>";
                        g = Math.floor(g * 1.25);
                    }
                    var h = document.getElementsByClassName("勝利_キャラ");
                    h[a].style.backgroundImage = "url(" + キャラ[編成中のキャラ[a]].img + ")";
                    document.getElementsByClassName("exp")[a].innerHTML = e.toLocaleString() + " <span style='color: green'>+ " + f.toLocaleString() + "</span>" + i + "<br>&emsp;&emsp;&emsp;⇒&nbsp;" + g.toLocaleString();
                    キャラ[編成中のキャラ[a]].EXP = g;
                    if (編成中のキャラ[a] == 0) {
                        キャラ[編成中のキャラ[a]].EXP = 0;
                        キャラ[編成中のキャラ[a]].Lv = 1;
                    }

                    var b = 100;
                    while (b > 0) {
                        if (level[b] <= キャラ[編成中のキャラ[a]].EXP) break;
                        b--;
                    }

                    var y = String(document.getElementsByClassName("exp")[a].innerHTML);
                    var x = 0;
                    if (b < 100) x = (level[b + 1] - キャラ[編成中のキャラ[a]].EXP);
                    document.getElementsByClassName("exp")[a].innerHTML = y + "<br>NEXT:&nbsp;" + x.toLocaleString();

                    var names = キャラ[編成中のキャラ[a]].名前;
                    if (言語 == "en") names = キャラ[編成中のキャラ[a]].name;
                    document.getElementsByClassName("v_name")[a].innerHTML = names;
                    var z = document.getElementsByClassName("level");
                    var str = "";
                    str += String(キャラ[編成中のキャラ[a]].Lv);
                    if (キャラ[編成中のキャラ[a]].Lv != b && 編成中のキャラ[a] != 0) {
                        var flg;
                        var asd = (Number(b) - キャラ[編成中のキャラ[a]].Lv);
                        if (asd > 0) asd = "+" + asd;
                        str = str + "&emsp;→&emsp;" + b + "&nbsp;<span style='color: green'>(" + asd + ")&emsp;</span><span style='color: red'>Level UP！</span>";
                        flg = true;
                        キャラ[編成中のキャラ[a]].Lv = Number(b);
                    }
                    z[a].innerHTML = str;
                    setTimeout(function() {
                        var aa = 0;
                        var str = "";
                        var name = [];
                        while (aa < キャラ.length) {
                            name[aa] = キャラ[aa].名前;
                            aa++;
                        }
                        aa = 0;
                        while (aa < 3) {
                            var a1 = name.indexOf(キャラ[編成中のキャラ[aa]].名前);
                            var a2 = 編成中のキャラ.indexOf(a1);
                            if (キャラ[編成中のキャラ[aa]].Lv != キャラ_戦闘[a2 + 3].Lv && 編成中のキャラ[aa] != 0) {
                                console.log("キャラ[編成中のキャラ[" + aa + "]].Lv : " + キャラ[編成中のキャラ[aa]].Lv + "\nb : " + b + "\n編成中のキャラ[" + aa + "] : " + 編成中のキャラ[aa]);
                                flg = true;
                            }
                            str += (キャラ[編成中のキャラ[aa]].Lv + " (" + b + ")  / ");
                            aa++;
                        }
                        if (flg == true) {
                            BGM.pause();
                            SE.src = "bgm/battle/lv_up.mp3";
                            SE.play();
                            setTimeout(function() {
                                if (現在画面 == "victory") bgm(現在画面);
                            }, 2500);
                        }
                    }, 510);
                    a++;
                }

                f = k;
                if (選択ステージ != 5) user_lank_EXP += Math.floor(f * 2.5 * 5);
                else user_lank_EXP += (f * 20 * 5);
                //if (user_lank < 15) user_lank_EXP *= 5;
                var b2 = 99;
                while (b2 > 0) {
                    if (level[b2] <= user_lank_EXP) break;
                    b2--;
                }
                user_lank = b2;
                document.getElementById("user_lank").innerHTML = user_lank;
                if (user_lank_EXP < level[99]) document.getElementById("user-rank").value = (user_lank_EXP / level[user_lank + 1]) * 100;
                else {
                    document.getElementById("user-rank").max = 100;
                    document.getElementById("user-rank").value = 100;
                }

                setTimeout(function() {
                    document.getElementById("battle").style.display = "none";
                    document.getElementById("victory").style.display = "block";
                    bgm(現在画面);
                }, 500);

                var a = 1;
                while (a < キャラ.length) {
                    console.log(キャラ[a].名前 + " : Lv." + キャラ[a].Lv + "\n");
                    if (キャラ[a].Lv != LvUp前キャラ_Lv[a]) {
                        var b = Number((1 + ((キャラ[a].Lv - 1) / 10)));
                        キャラ[a].HP = キャラ_初期値[a].HP * b;
                        キャラ[a].HP = Math.floor(キャラ[a].HP);
                        キャラ[a].MP = キャラ_初期値[a].MP * b;
                        キャラ[a].MP = Math.floor(キャラ[a].MP);
                        キャラ[a].ATK = キャラ_初期値[a].ATK * b;
                        キャラ[a].ATK = Math.floor(キャラ[a].ATK);
                        キャラ[a].MAT = キャラ_初期値[a].MAT * b;
                        キャラ[a].MAT = Math.floor(キャラ[a].MAT);
                        キャラ[a].DEF = キャラ_初期値[a].DEF * b;
                        キャラ[a].DEF = Math.floor(キャラ[a].DEF);
                        キャラ[a].MDF = キャラ_初期値[a].MDF * b;
                        キャラ[a].MDF = Math.floor(キャラ[a].MDF);
                        キャラ[a].SPD = キャラ_初期値[a].SPD * b;
                        キャラ[a].SPD = Math.floor(キャラ[a].SPD);
                        キャラ[a].LUK = キャラ_初期値[a].LUK * b;
                        キャラ[a].LUK = Math.floor(キャラ[a].LUK);
                    }
                    a++;
                }
            }, 100);
        }
        //味方全損
        var a = 0;
        var b = 0;
        var c = 0;
        while (a < (キャラ_戦闘.length / 2)) {
            b = Number(キャラ_戦闘[a].HP);
            if (b < 0) b = 0;
            c += b;
            a++;
        }
        if (c == 0) {
            if (現在画面 == "battle") {
                現在画面 = "battle2";
                setTimeout(function() {
                    現在画面 = "select";
                    bgm("select");
                    btm_ステージ();
                }, 1000);
            }
        }
    }
}

function 戦闘開始時処理_配列入れ替え() {
    var b = 0;
    var c = 0;
    while (c < 1000) {
        b = 0;
        while (b < キャラSPD[4].length) {
            var tmp1 = 0;
            var tmp2 = "";
            if (Number(キャラSPD[4][b]) == -1 && (Number(b + 1) < Number(キャラSPD[4].length))) {
                tmp1 = キャラSPD[4][b];
                キャラSPD[4][b] = キャラSPD[4][b + 1];
                キャラSPD[4][b + 1] = tmp1;
                tmp2 = キャラSPD[5][b];
                キャラSPD[5][b] = キャラSPD[5][b + 1];
                キャラSPD[5][b + 1] = tmp2;
            }
            b++;
        }
        c++;
    }

    if (現在画面 != "select") {
        setTimeout(function() {
            戦闘開始時処理_配列入れ替え();
        }, 10);
    }
}

function 戦闘開始時処理_ログ表示() {
    var str = "";
    var a = 0;
    var b = [...キャラSPD[4]];
    var num = 45;
    var ソート後 = true;
    if (num == 4 || num == 45) {
        while (a < キャラSPD[4].length) {
            /*if (キャラSPD[4][a] == 攻撃1_a) str += "キャラSPD[4][" + a + "]：■■" + キャラSPD[4][a] + "■■\n";
            else */
            str += "キャラSPD[4][" + a + "]：" + キャラSPD[4][a] + "\n";
            a++;
        }
        if (ソート後 == true) {
            b = [...ソート(b, "昇順")];
            str += "\n\n\n";
            a = 0;
            while (a < b.length) {
                /*if (キャラSPD[4][a] == 攻撃1_a) str += "キャラSPD[4][" + a + "]：■■" + キャラSPD[4][a] + "■■\n";
                else */
                str += "キャラSPD[4][" + a + "]：" + b[a] + "\n";
                a++;
            }
        }
    }
    a = 0;
    if (num == 45) str += "\n\n\n";
    if (num == 5 || num == 45) {
        while (a < キャラSPD[5].length) {
            if (キャラSPD[5][a] == "ヤマト") str += "キャラSPD[5][" + a + "]：■■■■■\n";
            else str += "キャラSPD[5][" + a + "]：" + キャラSPD[5][a] + "\n";
            a++;
        }
    }
    //敵速度表示関連(textarea)
    document.getElementById("logs").value = str;

    if (現在画面 != "select") {
        setTimeout(function() {
            戦闘開始時処理_ログ表示();
        }, 10);
    }
}

function 行動サイクル1(c) {
    var bk_num = キャラSPD[4][0];
    var bk_name = キャラSPD[5][0];
    var a = Number(キャラSPD[4].length) - 1;
    var b = 0;
    b = 0;
    if (c <= 0) c = a;
    while (b < c) {
        キャラSPD[4][b] = キャラSPD[4][b + 1];
        キャラSPD[5][b] = キャラSPD[5][b + 1];
        b++;
    }
    if ((c - 1) >= 0) c--;
    キャラSPD[5][c] = bk_name;
    キャラSPD[4][c] = bk_num;

    var a = 0;
    var bk_char = [
        //player
        [],
        //enemy
        [],
        //total
        []
    ];

    while (a < キャラ.length) {
        bk_char[0][a] = キャラ[a].名前;
        bk_char[2][a] = キャラ[a].img;
        a++;
    }
    a = 0;
    while (a < 敵キャラ.length) {
        bk_char[1][a] = 敵キャラ[a].名前;
        bk_char[2][Number(a + キャラ.length)] = 敵キャラ[a].img;
        a++;
    }

    var a = キャラSPD[5][0];
    var b = bk_char[0].indexOf(a);
    if (b >= 0) document.getElementById("play_char").style.backgroundImage = "url(" + bk_char[2][b] + ")";

    行動順表示();
}

function battle_error(str) {
    alert(str);
}

var 攻撃1_a = 0;

function 攻撃1() {
    行動サイクル2();
}

function 攻撃2() {
    攻撃1_a = 0;
}

function 行動サイクル2() {
    攻撃1_a++;
    /*
    if (攻撃1_a == 3) {
        //敵を倒した時の順序整理処理
        /
        var a = 1;
        キャラSPD[4][a] = -1;
        キャラSPD[5][a] = null;
        /
    }
    */
    var b = 0;
    var c = 0;
    while (c < 1000) {
        b = 0;
        while (b < キャラSPD[4].length) {
            var tmp1 = 0;
            var tmp2 = "";
            if (Number(キャラSPD[4][b]) == -1 && (Number(b + 1) < Number(キャラSPD[4].length))) {
                tmp1 = キャラSPD[4][b];
                キャラSPD[4][b] = キャラSPD[4][b + 1];
                キャラSPD[4][b + 1] = tmp1;
                tmp2 = キャラSPD[5][b];
                キャラSPD[5][b] = キャラSPD[5][b + 1];
                キャラSPD[5][b + 1] = tmp2;
            }
            b++;
        }
        c++;
    }
    c = 0;

    var f = 0;
    var e = 1;
    var a = Number(キャラSPD[4].length) - 1;
    var b = 0;
    var c = 0;
    while (b < a) {
        if (キャラSPD[4][b] >= 0) c++;
        b++;
    }
    行動サイクル1(c + 1);
    プレイヤー判定();
}

function プレイヤー判定() {
    if (現在画面 == "battle") {
        var a = 1;
        var b = [];
        while (a < キャラ.length) {
            b[a - 1] = キャラ[a].名前;
            a++;
        }
        //敵の行動処理
        var c = b.indexOf(キャラSPD[5][0]); //-1なら敵キャラ
        if (c < 0) { //敵キャラならば
            user = false;
            battle_user = false;

            var a = 0;
            var b = [];
            while (a < 敵キャラ_ローカル.length) {
                b[a] = 敵キャラ_ローカル[a].名前;
                a++;
            }
            var 敵キャラ_添え字 = b.indexOf(キャラSPD[5][0]);
            var 攻撃手段 = run(4, 1);
            var 攻撃対象 = run(2, 0);
            var a = 敵キャラ_ローカル[敵キャラ_添え字].MP;
            var b = a;
            if (攻撃手段 == 3) b = Math.floor(b / 2);
            a -= run(b, 1);
            if (a < 0) 攻撃手段 = 1;
            else 敵キャラ_ローカル[敵キャラ_添え字].MP = a;
            var a = Math.abs(Number(ダメージ(キャラSPD[5][0], 編成中のキャラ_名前[攻撃対象], 攻撃手段)));
            var str1 = 敵キャラ_ローカル[敵キャラ_添え字].名前 + "の" + 編成中のキャラ_名前[攻撃対象] + "への攻撃";
            var str2 = 編成中のキャラ_名前[攻撃対象] + "に" + Math.floor(a).toLocaleString() + "ダメージ";
            switch (攻撃手段) {
                case 1:
                    //a /= 1000;
                    break;
            }
            if (a == "NaN" || Math.floor(a) == 0 || 編成中のキャラ_名前[攻撃対象] == "" || キャラ_戦闘[攻撃対象].HP <= 0) str2 = "ミス！" + a;
            else キャラ_戦闘[攻撃対象].HP -= Math.floor(a);
            if (キャラ_戦闘[攻撃対象].HP < 0) キャラ_戦闘[攻撃対象].HP = 0;
            スキル表示(str1, str2);
            setTimeout(function() {
                戦闘中_ステータス表示();
                行動サイクル2();
            }, (戦闘スピード * 3500));
        } else user = true;
    }
}

function 敵行動() {}

function 敵選択(num) {
    if (敵キャラ_ローカル[(敵配置[選択ステージ][num + 1])].HP > 0) {
        var a = document.getElementsByClassName("戦闘画面_敵選択_div");
        var b = 0;
        while (b < a.length) {
            if (num == 3) a[b].style.display = "block";
            else a[b].style.display = "none";
            b++;
        }
        if (num < 4) a[num].style.display = "block";
        戦闘_敵選択 = num;
    }
}

function 戦闘終了_ステージ選択() {
    var a = 0;
    現在画面 = "select";
    btm_ステージ();
    bgm(現在画面);
}


function 攻撃(num) {
    if (user && battle_user && 現在画面 == "battle") {
        battle_user = false;
        var mob = 戦闘_敵選択;
        var mob2 = mob + 1;
        var 消費 = 0;
        var 対象キャラ = 編成中のキャラ_名前.indexOf(キャラSPD[5][0]);
        var 攻撃種類 = 攻撃種類_判別(キャラSPD[5][0], num);
        /*
            1 : 通常攻撃
            2 : 強攻撃
            3 : 魔法
            4 : 大魔法
            5 : 防御

            敵キャラ_ローカル[mob].HP
        */
        消費 = MP消費(num, キャラ_戦闘[対象キャラ].MP);
        if (対象キャラ >= 0) キャラ_戦闘[対象キャラ].MP -= 消費;
        if (対象キャラ < 0 || キャラ_戦闘[対象キャラ].MP >= 0 || 戦闘_flg == true) {
            var a = Math.abs(Number(ダメージ(キャラSPD[5][0], 戦闘_敵選択_名前[mob], num)));
            a = 攻撃力補正(num, a);
            var str1 = キャラSPD[5][0] + "の" + 攻撃種類;
            var str2 = 敵キャラ_ローカル[(敵配置[選択ステージ][mob + 1])].名前 + "に" + a.toLocaleString() + "ダメージ";
            if ((a == "NaN" || a == 0 || 戦闘_敵選択_名前[mob] == "" || 敵キャラ_ローカル[(敵配置[選択ステージ][mob + 1])].HP == 0) && 攻撃種類 == "攻撃") str2 = "ミス！";
            switch (攻撃種類) {
                case "攻撃":
                    敵キャラ_ローカル[(敵配置[選択ステージ][mob + 1])].HP -= a;
                    switch (キャラSPD[5][0]) {
                        case "ありす":
                        case "ナハス":
                        case "エア":
                        case "ネイト":
                        case "坂本":
                            if (敵キャラ_ローカル[(敵配置[選択ステージ][mob + 1])].HP > 0) {
                                var r = run(100, 0);
                                if (r < 20) {
                                    var 相手MP = 敵キャラ_ローカル[(敵配置[選択ステージ][mob + 1])].MP;
                                    var a = Math.floor((相手MP * (キャラ_戦闘[対象キャラ].Lv / 500)) * num);
                                    敵キャラ_ローカル[(敵配置[選択ステージ][mob + 1])].MP -= a;
                                    キャラ_戦闘[対象キャラ].MP += a;
                                }
                            }
                            break;

                        default:
                            break;
                    }
                    break;

                case "魔法":
                    if (敵キャラ_ローカル[(敵配置[選択ステージ][mob + 1])].HP > 0) {
                        敵キャラ_ローカル[(敵配置[選択ステージ][mob + 1])].HP -= a;
                    }
                    break;

                case "HP回復":
                    a = 全体HP回復(num);
                    str2 = "味方全員に" + 攻撃種類;
                    break;

                case "MP回復":
                    全体MP回復(a, num);
                    str2 = "味方全員に" + 攻撃種類;
                    break;

                default:
                    str2 = "味方全員に" + 攻撃種類;
                    break;
            }
            if (敵キャラ_ローカル[mob + 1].HP < 0) 敵キャラ_ローカル[mob + 1].HP = 0;
            スキル表示(str1, str2);
            setTimeout(function() {
                戦闘中_ステータス表示();
                行動サイクル2();

                //敵を倒したら自動で敵選択を変更する
                var a = 敵キャラ_ローカル[mob + 1].HP;
                if (a <= 0) {
                    //もし敵を倒したら
                    var c = []; //倒してない敵リストアップ用
                    var d = 0;
                    var e = 0;
                    while (d < 3) {
                        var f = 敵キャラ_ローカル[(敵配置[選択ステージ][d + 1])].HP;
                        if (f > 0) {
                            //倒した敵じゃなければ
                            c[e] = d;
                            e++;
                        }
                        d++;
                    }
                    if (c.length > 0) {
                        var g = run((c.length - 1), 0);
                        敵選択(c[g]);
                    }
                }
            }, (戦闘スピード * 3500));
        } else {
            戦闘_flg = true;
            se("戻る");
            スキル表示("MPが" + (消費 - キャラ_戦闘[対象キャラ].MP).toLocaleString() + "足りません", "");
            if (対象キャラ >= 0) キャラ_戦闘[対象キャラ].MP += 消費;
        }
    }
}

function 攻撃種類_判別(キャラ名, 選択コマンド) {
    var 変数類 = [キャラ名, 選択コマンド];
    var 攻撃種類 = "";
    switch (変数類.toString()) {
        case ["ありす", 3].toString():
        case ["ありす", 4].toString():
            攻撃種類 = "魔法"
            break;

        case ["ナハス", 3].toString():
        case ["ナハス", 4].toString():
            攻撃種類 = "HP回復";
            break;

        case ["ネイト", 3].toString():
            攻撃種類 = "魔法";
            break;

        case ["ネイト", 4].toString():
            攻撃種類 = "MP回復";
            break;

        default:
            攻撃種類 = "攻撃";
            break;
    }
    return 攻撃種類;
}

function 全体HP回復(num) {
    var a = 編成中のキャラ_名前.indexOf(キャラSPD[5][0]);
    var b = 0;
    var c = 0;
    var d = 0;
    var e = 0;
    while (b < 3) {
        if (編成中のキャラ[b] != 0 && キャラ_戦闘[b].HP < 99999) {
            c = キャラ_戦闘[b + 3].HP * (キャラ_戦闘[b].Lv / 10) * (1 + ((num * 2) / 10)) / ((10 - (num * 2)) * 10) * 3;
            d += Math.abs(c);
            if (キャラ_戦闘[b].HP > 0) e++;
            キャラ_戦闘[b].HP += Math.floor(Math.abs(c));
            if (キャラ_戦闘[b].HP > 99999) キャラ_戦闘[b].HP = 99999;
        }
        b++;
    }
    return Math.floor(d / e);
}

function 全体MP回復(num) {
    var a = 編成中のキャラ_名前.indexOf(キャラSPD[5][0]);
    var b = 0;
    var c = 0;
    var d = 0;
    var e = 0;
    while (b < 3) {
        if (編成中のキャラ[b] != 0 && キャラ_戦闘[b].MP < 99999) {
            c = キャラ_戦闘[b + 3].HP * (キャラ_戦闘[b].Lv / 10) * (1 + ((num * 2) / 10)) / ((10 - (num * 2)) * 10) * 3;
            d += Math.abs(c);
            if (キャラ_戦闘[b].MP > 0) e++;
            キャラ_戦闘[b].MP += Math.floor(Math.abs(c));
            if (キャラ_戦闘[b].MP > 99999) キャラ_戦闘[b].MP = 99999;
        }
        b++;
    }
    return Math.floor(d / e);
}

function MP消費(num, MP値) {
    var 変数類 = [キャラSPD[5][0], num];
    var 消費 = 0;
    var a = 戦闘_レベル[0][戦闘_レベル[1].indexOf(変数類[0])];
    var b = 0;
    var c = [];
    while (b < キャラ_戦闘.length) {
        c[b] = キャラ_戦闘[b].名前;
        b++;
    }
    switch (変数類.toString()) {
        case ["ありす", 3].toString():
        case ["エア", 3].toString():
        case ["アミー", 3].toString():
            消費 = Math.floor(AID("MP", 2) * 0.3);
            break;

        case ["ありす", 4].toString():
        case ["エア", 4].toString():
        case ["ナハス", 4].toString():
        case ["アミー", 4].toString():
            消費 = Math.floor(AID("MP", 2) * 0.8);
            break;

        case ["ネイト", 4].toString():
            var 消費割合 = 50;
            消費 = キャラ_戦闘[c.indexOf("ネイト") + 3].MP * (消費割合 / 100);
            if (Math.floor(消費) > (キャラ_戦闘[c.indexOf("ネイト")].MP * (消費割合 / 100))) 戦闘_flg = false;
            setTimeout(function() {
                キャラ_戦闘[c.indexOf("ネイト")].MP = Math.floor(キャラ_戦闘[c.indexOf("ネイト") + 3].MP * ((100 - 消費割合) / 100));
                戦闘中_ステータス表示();
            }, 5);
            break;

        default:
            消費 = 0;
            if (num == 3) 消費 = Math.floor(AID("MP", 2) * 0.1);
            if (num == 4) 消費 = Math.floor(AID("MP", 2) * 0.3);
            break;
    }
    if (変数類[1] == 2) {
        消費 = Math.floor(AID("MP", 2) * 0.25);
        if (消費 < 2) 消費 = 2;
    }
    if (MP値 < 消費) 戦闘_flg = false;
    return Number(消費);
}

function 攻撃力補正(num, a) {
    var 攻撃力 = a;
    var 変数類 = [キャラSPD[5][0], num];

    if ((num != 1) && (攻撃力 == 0)) 攻撃力 = 1;

    switch (変数類.toString()) {
        case ["坂本", 2].toString():
            攻撃力 *= 5;
            break;

        case ["坂本", 4].toString():
            攻撃力 = AID("ATK") + AID("SPD");
            break;

        case ["ありす", 4].toString():
            攻撃力 *= 10;
            break;

        case ["エア", 4].toString():
            攻撃力 *= 7.5;
            break;

        case ["ナハス", 4].toString():
            攻撃力 *= 5;
            break;

        case ["ヴァルター", 2].toString():
            攻撃力 = AID("ATK") / 3.25;
            break;

        case ["ヴァルター", 4].toString():
            攻撃力 = AID("MAT") / 2;
            break;

        default:
            if (num == 2) 攻撃力 *= 2.5;
            break;
    }
    return Math.floor(Number(攻撃力) * 攻撃力_バフ);
}

function AID(str1, str2) {
    var 行動キャラ添え字 = 編成中のキャラ_名前.indexOf(キャラSPD[5][0]);
    var a = ["Lv", "HP", "MP", "ATK", "MAT", "SPD"];
    var status = [
        ["Lv", キャラ_戦闘[行動キャラ添え字].Lv, キャラ_戦闘[行動キャラ添え字 + 3].Lv],
        ["HP", キャラ_戦闘[行動キャラ添え字].HP, キャラ_戦闘[行動キャラ添え字 + 3].HP],
        ["MP", キャラ_戦闘[行動キャラ添え字].MP, キャラ_戦闘[行動キャラ添え字 + 3].MP],
        ["ATK", キャラ_戦闘[行動キャラ添え字].ATK, キャラ_戦闘[行動キャラ添え字 + 3].ATK],
        ["MAT", キャラ_戦闘[行動キャラ添え字].MAT, キャラ_戦闘[行動キャラ添え字 + 3].MAT],
        ["SPD", キャラ_戦闘[行動キャラ添え字].SPD, キャラ_戦闘[行動キャラ添え字 + 3].SPD],
    ];
    var b = a.indexOf(str1);
    if (b >= 0) {
        if (str2 == undefined) return Number(status[b][1]);
        else return Number(status[b][2]);
    } else return undefined;
}

function ダメージ(攻撃者, 対象, 行動) {
    /*
        1 : 通常攻撃
        2 : 強攻撃
        3 : 魔法
        4 : 大魔法
        5 : 防御

        敵キャラ_ローカル[mob].HP
    */

    //キャラの名前を１つの配列に
    var name = [];
    var char = [{
            HP: 0,
            MP: 0,
            ATK: 0,
            MAT: 0,
            DEF: 0,
            MDF: 0,
            SPD: 0,
            LUK: 0
        },
        {
            HP: 0,
            MP: 0,
            ATK: 0,
            MAT: 0,
            DEF: 0,
            MDF: 0,
            SPD: 0,
            LUK: 0
        }
    ];
    var a = 0;
    var b = 0;
    while (a < (キャラ.length - 1)) {
        name[a] = String(キャラ[a + 1].名前);
        a++;
    }
    while (b < (敵キャラ.length - 1)) {
        name[a] = 敵キャラ[b + 1].名前;
        a++;
        b++;
    }
    var c = Number(name.indexOf(攻撃者));
    if (c < (キャラ.length - 1)) {
        //プレイヤーならば
        c++;
        char[0].HP = キャラ[c].HP;
        char[0].MP = キャラ[c].MP;
        char[0].ATK = キャラ[c].ATK;
        char[0].MAT = キャラ[c].MAT;
        char[0].DEF = キャラ[c].DEF;
        char[0].MDF = キャラ[c].MDF;
        char[0].SPD = キャラ[c].SPD;
        char[0].LUK = キャラ[c].LUK;
    } else {
        //敵キャラならば
        c -= (キャラ.length - 1);
        c++;
        char[0].HP = 敵キャラ_ローカル[c].HP;
        char[0].MP = 敵キャラ_ローカル[c].MP;
        char[0].ATK = 敵キャラ_ローカル[c].ATK;
        char[0].MAT = 敵キャラ_ローカル[c].MAT;
        char[0].DEF = 敵キャラ_ローカル[c].DEF;
        char[0].MDF = 敵キャラ_ローカル[c].MDF;
        char[0].SPD = 敵キャラ_ローカル[c].SPD;
        char[0].LUK = 敵キャラ_ローカル[c].LUK;
    }

    var d = Number(name.indexOf(対象));
    if (d < (キャラ.length - 1)) {
        //プレイヤーならば
        d++;
        char[1].HP = キャラ[d].HP;
        char[1].MP = キャラ[d].MP;
        char[1].ATK = キャラ[d].ATK;
        char[1].MAT = キャラ[d].MAT;
        char[1].DEF = キャラ[d].DEF;
        char[1].MDF = キャラ[d].MDF;
        char[1].SPD = キャラ[d].SPD;
        char[1].LUK = キャラ[d].LUK;
    } else {
        //敵キャラならば
        d -= (キャラ.length - 1);
        d++;
        char[1].HP = 敵キャラ_ローカル[d].HP;
        char[1].MP = 敵キャラ_ローカル[d].MP;
        char[1].ATK = 敵キャラ_ローカル[d].ATK;
        char[1].MAT = 敵キャラ_ローカル[d].MAT;
        char[1].DEF = 敵キャラ_ローカル[d].DEF;
        char[1].MDF = 敵キャラ_ローカル[d].MDF;
        char[1].SPD = 敵キャラ_ローカル[d].SPD;
        char[1].LUK = 敵キャラ_ローカル[d].LUK;
    }

    var C3 = char[0].HP;
    var C4 = char[0].MP;
    var C5 = char[0].ATK;
    var C6 = char[0].MAT;
    var C7 = char[0].DEF;
    var C8 = char[0].MDF;
    var C9 = char[0].SPD;
    var C10 = char[0].LUK;

    var N3 = char[1].HP;
    var N4 = char[1].MP;
    var N5 = char[1].ATK;
    var N6 = char[1].MAT;
    var N7 = char[1].DEF;
    var N8 = char[1].MDF;
    var N9 = char[1].SPD;
    var N10 = char[1].LUK;

    if (行動 == 1 || 行動 == 2) {
        var E4 = C5 * (C10 / 100);
        var G4 = E4 * 1.5;

        //P-ATK
        var E10 = C5 * (E4 / 100);
        if (行動 == 2) E10 = C5 * (G4 / 100);

        var K21 = E10;


        //ATK
        var C21 = K21;
        var E21 = N5;
        var G21 = C21 / E21;
        G21 = G21 * 100;
        G21 = Math.floor(G21);
        G21 = G21 / 100;


        //DEF
        var C24 = C7;
        var E24 = N7;
        var G24 = C24 / E24;
    } else {
        var I4 = C6 * (C10 / 100);
        var K4 = I4 * 1.5;

        //P-MAT
        var I10 = C6 * (I4 / 100);
        if (行動 == 4) I10 = C6 * (K4 / 100);

        var K21 = I10;

        //MAT
        var C21 = K21;
        var E21 = N6;
        var G21 = C21 / E21;

        //MDF
        var C24 = C8;
        var E24 = N8;
        var G24 = C24 / E24;
    }


    //SPD
    var C27 = C9;
    var E27 = N9;
    var G27 = C27 / E27;


    //LUK
    var C30 = C10;
    var E30 = N10;
    var G30 = C30 / E30;


    //最終計算
    var C18 = K21;
    var E18 = 100 - G21;
    var G18 = G27;
    var I18 = G30;
    var K18 = G24;
    var M17 = Math.floor(C18 / E18 * G18 * I18 * K18);

    var a = run(char[0].LUK, 0);
    SE = new Audio();
    var c = "bgm/battle/";
    //会心の一撃
    if ((a < (char[0].LUK / 10)) && 行動 < 3) {
        switch (難易度) {
            case "EASY":
                M17 *= 15;
                break;

            case "NORMAL":
                M17 *= 10;
                break;

            case "HARD":
                M17 *= 5;
                break;

            case "EXTRA":
                M17 *= 2.5;
                break;

            default:
                break;
        }
        SE.src = c + "kaisin.mp3";
    } else {
        switch (行動) {
            case 1:
            case 2:
                SE.src = c + "ken.mp3";
                break;

            case 3:
                SE.src = c + "flaim.ogg";
                break;

            case 4:
                SE.src = c + "d_flaim.ogg";
                break;

            default:
                break;
        }
        var a = [攻撃者, 行動];
        switch (a.toString()) {
            case ["アミー", 1].toString():
            case ["アミー", 2].toString():
                SE.src = c + "kobusi.ogg";
                break;

            case ["ナハス", 3].toString():
            case ["ナハス", 4].toString():
            case ["ネイト", 4].toString():
                SE.src = c + "LaboHeal.ogg";
                break;

            case ["エア", 3].toString():
            case ["エア", 4].toString():
                SE.src = c + "LaboWind2.ogg";
                break;

            default:
                break;
        }
    }
    SE.play();
    //document.title = a + " / " + (char[0].LUK / 10) + " / " + (a < (char[0].LUK / 10)) + " / " + M17;
    return M17;
}

function スキル表示(str, str2) {
    var a = document.getElementById("スキル");
    var time = 1.5 / 戦闘スピード; //秒指定
    a.innerHTML = str;
    setTimeout(function() {
        a.innerHTML = str2;
        if (str2 == "") battle_user = true;
    }, (time * 1000));
    if (str2 != "") {
        setTimeout(function() {
            a.innerHTML = "";
            battle_user = true;
        }, (time * 2000));
    }
}








function 名前(num) {
    var str = "";
    switch (num) {
        case 1:
            str = "坂本";
            break;

        case 2:
            str = "ありす";
            break;

        case 3:
            str = "アレス";
            break;

        case 4:
            str = "アミー";
            break;

        default:
            break;
    }
    return str;
}

function 逃げる() {
    var b = ["ステージ選択に戻りますか？", "Would you like to return to stage selection?"];
    var c = 0;
    if (言語 == "en") c = 1;
    確認画面("逃げる", b[c]);
}