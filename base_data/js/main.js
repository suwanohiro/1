//ユーザーランクバーの表示がが反映されていないので確認

var build_type = "debug"; //debug or release
var dis_id = ["title", "select", "btm", "config", "battle", "battle2", "victory", "編成", "確認画面", "操作説明", "admin", "ending"];
var 表示ID = "title";
var 現在画面 = 表示ID;
var コイン = 500;
var MASTER = 0;
var BGM = new Audio();
var SE = new Audio();
var 音量 = [100, 100, 100];
var 言語 = "jp";
var 起動ロード時間 = 6;
var select_キャラ_a = 1;
var select_キャラ_b = 2;
var ステージ = 0;
var 選択ステージ = 5;
var 編成中のキャラ = [2, 6, 9];
var 編成中のキャラ_名前 = ["", "", ""];
var ステージ選択_コントローラー選択_num = -1;
var LoSt = [
    "lang"
];
var 敵選択_透明度 = [false, 100];
var user = true;
var battle_user = true;
var キャラSPD = [
    [0, 0, 0, 0, 0, 0],
    ["", "", "", "", "", ""],
    [0, 0, 0, 0, 0, 0],
    ["", "", "", "", "", ""],
    [0, 0, 0, 0, 0, 0],
    ["", "", "", "", "", ""]
];
var 戦闘_敵選択 = 2 - 1;
var 戦闘_敵選択_名前 = ["", "", ""];
var 戦闘_レベル = [
    [],
    []
];
var 超過分 = [
    //HP
    [0, 0, 0],
    //MP
    [0, 0, 0]
];
var 敵情報 = [
    //HP
    [0, 0, 0],
    //MP
    [0, 0, 0]
];
var 戦闘_flg = true;
var 確認画面_画面情報 = "";
var 確認画面_処理判別 = "";
var 操作説明_画面情報 = "";
var Commands = "";
var user_lank = 1;
var user_lank_EXP = 0;
var user_lank_EXP_p = 0;
var 攻撃力_バフ = 2;
var 戦闘スピード = 1;
var admin_char_Lv = [
    1, //坂本
    1, //ありす
    1, //アレス
    1, //アミー
    1, //ナハス
    1, //エア
    1, //ネイト
    1, //ディアーナ
    1 //ヴァルター
];

//EASY - NORMAL - HARD - EXTRA
var 難易度 = "NORMAL";

function 初期設定() {
    //音量設定
    //マスター
    document.getElementById("master").value = 音量[0];
    //BGM
    document.getElementById("volume").value = 音量[1];
    //SE
    document.getElementById("se").value = 音量[2];

    開発中用初期設定();

    //キャラのステータス値設定
    var a = 1;
    while (a < キャラ.length) {
        キャラ[a].HP *= (1 + ((キャラ[a].Lv - 1) / 10));
        キャラ[a].HP = Math.floor(キャラ[a].HP);
        キャラ[a].MP *= (1 + ((キャラ[a].Lv - 1) / 10));
        キャラ[a].MP = Math.floor(キャラ[a].MP);
        キャラ[a].ATK *= (1 + ((キャラ[a].Lv - 1) / 10));
        キャラ[a].ATK = Math.floor(キャラ[a].ATK);
        キャラ[a].MAT *= (1 + ((キャラ[a].Lv - 1) / 10));
        キャラ[a].MAT = Math.floor(キャラ[a].MAT);
        キャラ[a].DEF *= (1 + ((キャラ[a].Lv - 1) / 10)) * 10;
        キャラ[a].DEF = Math.floor(キャラ[a].DEF);
        キャラ[a].MDF *= (1 + ((キャラ[a].Lv - 1) / 10)) * 10;
        キャラ[a].MDF = Math.floor(キャラ[a].MDF);
        キャラ[a].SPD *= (1 + ((キャラ[a].Lv - 1) / 10));
        キャラ[a].SPD = Math.floor(キャラ[a].SPD);
        キャラ[a].LUK *= (1 + ((キャラ[a].Lv - 1) / 10));
        キャラ[a].LUK = Math.floor(キャラ[a].LUK);
        a++;
    }

    a = 0;
    while (a < 3) {
        var b = 1;
        while (b < キャラ.length) {
            if (編成中のキャラ[a] == キャラ[b].num) {
                document.getElementsByClassName("編成_キャラ")[a + 9].style.backgroundImage = "url(" + キャラ[b].img + ")";
                break;
            }
            b++;
        }
        a++;
    }

    a = 0;
    while (a < 敵キャラ.length) {
        敵キャラ_ローカル[a].num = 敵キャラ[a].num;
        敵キャラ_ローカル[a].img = 敵キャラ[a].img;
        敵キャラ_ローカル[a].名前 = 敵キャラ[a].名前;
        敵キャラ_ローカル[a].name = 敵キャラ[a].name;
        敵キャラ_ローカル[a].Lv = 敵キャラ[a].Lv;
        敵キャラ_ローカル[a].HP = 敵キャラ[a].HP;
        敵キャラ_ローカル[a].MP = 敵キャラ[a].MP;
        敵キャラ_ローカル[a].ATK = 敵キャラ[a].ATK;
        敵キャラ_ローカル[a].MAT = 敵キャラ[a].MAT;
        敵キャラ_ローカル[a].DEF = 敵キャラ[a].DEF;
        敵キャラ_ローカル[a].MDF = 敵キャラ[a].MDF;
        敵キャラ_ローカル[a].SPD = 敵キャラ[a].SPD;
        敵キャラ_ローカル[a].LUK = 敵キャラ[a].LUK;
        敵キャラ_ローカル[a].EXP = 敵キャラ[a].EXP;
        a++;
    }

    敵選択(戦闘_敵選択);


    //エンディング背景動画再生速度変更
    //document.getElementById("ending_movie").playbackRate = 0.1;

    if (表示ID == "battle") 戦闘画面へ();
    if (表示ID == "ending") ending();
    ステージ選択_コントローラー選択(1);
}

function 開発中用初期設定() {
    if (build_type == "debug") {
        var a = 0;
        while (a < admin_char_Lv.length) {
            キャラ[a + 1].Lv = admin_char_Lv[a];
            a++;
        }
    }
}

function 情報保存() {
    //0 : 表示ID

    var str = [];

    str[0] = 表示ID;
    str[1] = 音量[0]; //master
    str[2] = 音量[1]; //volume
    str[3] = 音量[2]; //se
    str[4] = 編成中のキャラ[0]; //01
    str[5] = 編成中のキャラ[1]; //02
    str[6] = 編成中のキャラ[2]; //03
    str[7] = 難易度;
    str[8] = 言語;
    str[9] = 起動ロード時間;
    str[10] = 現在画面;
    str[11] = user;
    str[12] = user_lank; //ユーザーランク
    str[13] = user_lank_EXP; //ユーザーランク_EXP
    str[14] = Commands; //コマンド
    str[15] = 選択ステージ;
    str[16] = 確認画面_画面情報;
    str[17] = 確認画面_処理判別;
    str[18] = 操作説明_画面情報;
    str[19] = 敵キャラ_ローカル[(敵配置[選択ステージ][1])].HP;
    str[20] = 敵キャラ_ローカル[(敵配置[選択ステージ][2])].HP;
    str[21] = 敵キャラ_ローカル[(敵配置[選択ステージ][3])].HP;
    str[22] = キャラ_戦闘[0].HP;
    str[23] = キャラ_戦闘[1].HP;
    str[24] = キャラ_戦闘[2].HP;
    str[25] = "キャラ.length";
    str[26] = キャラ.length;
    var a = str.length;
    var b = a;
    while ((a - b) < キャラ.length) {
        str[a] = キャラ系配列結合("キャラ", (a - b));
        a++;
    }
    str[a] = キャラ_初期値.length;
    a++;
    var a = str.length;
    var b = a;
    while ((a - b) < キャラ_初期値.length) {
        str[a] = キャラ系配列結合("キャラ_初期値", (a - b));
        a++;
    }
    str[a] = 敵キャラ.length;
    a++;
    var a = str.length;
    var b = a;
    while ((a - b) < 敵キャラ.length) {
        str[a] = キャラ系配列結合("敵キャラ", (a - b));
        a++;
    }
    str[a] = 敵キャラ.length;
    a++;
    var a = str.length;
    var b = a;
    while ((a - b) < 敵キャラ_ローカル.length) {
        str[a] = キャラ系配列結合("敵キャラ_ローカル", (a - b));
        a++;
    }
    str[a] = 戦闘_敵選択_名前[0];
    a++;
    str[a] = 戦闘_敵選択_名前[1];
    a++;
    str[a] = 戦闘_敵選択_名前[2];
    a++;

    var nowTime = new Date();
    var nowHour = nowTime.getHours();
    if (nowHour < 10) nowHour = "0" + nowHour;
    var nowMin = nowTime.getMinutes();
    if (nowMin < 10) nowMin = "0" + nowMin;
    var nowSec = nowTime.getSeconds();
    if (nowSec < 10) nowSec = "0" + nowSec;
    var nowYear = nowTime.getFullYear();
    var nowMonth = nowTime.getMonth();
    nowMonth = nowMonth + 1;
    if (nowMonth < 10) nowMonth = "0" + nowMonth;
    var nowDate = nowTime.getDate();
    if (nowDate < 10) nowDate = "0" + nowDate;
    msg = String(nowYear) + String(nowMonth) + String(nowDate) + String(nowHour) + String(nowMin) + String(nowSec);
    localStorage.game_save_data = String(str) + "," + msg;
}

function キャラ系配列結合(str, num) {
    var char = "";
    if (str == "キャラ") {
        char += String(キャラ[num].num) + "'";
        char += String(キャラ[num].img) + "'";
        char += String(キャラ[num].名前) + "'";
        char += String(キャラ[num].name) + "'";
        char += String(キャラ[num].Lv) + "'";
        char += String(キャラ[num].HP) + "'";
        char += String(キャラ[num].MP) + "'";
        char += String(キャラ[num].ATK) + "'";
        char += String(キャラ[num].MAT) + "'";
        char += String(キャラ[num].DEF) + "'";
        char += String(キャラ[num].MDF) + "'";
        char += String(キャラ[num].SPD) + "'";
        char += String(キャラ[num].LUK) + "'";
        char += String(キャラ[num].EXP);
    } else if (str == "キャラ_初期値") {
        char += String(キャラ_初期値[num].num) + "'";
        char += String(キャラ_初期値[num].img) + "'";
        char += String(キャラ_初期値[num].名前) + "'";
        char += String(キャラ_初期値[num].name) + "'";
        char += String(キャラ_初期値[num].Lv) + "'";
        char += String(キャラ_初期値[num].HP) + "'";
        char += String(キャラ_初期値[num].MP) + "'";
        char += String(キャラ_初期値[num].ATK) + "'";
        char += String(キャラ_初期値[num].MAT) + "'";
        char += String(キャラ_初期値[num].DEF) + "'";
        char += String(キャラ_初期値[num].MDF) + "'";
        char += String(キャラ_初期値[num].SPD) + "'";
        char += String(キャラ_初期値[num].LUK) + "'";
        char += String(キャラ_初期値[num].EXP);
    } else if (str == "敵キャラ") {
        char += String(敵キャラ[num].num) + "'";
        char += String(敵キャラ[num].img) + "'";
        char += String(敵キャラ[num].名前) + "'";
        char += String(敵キャラ[num].name) + "'";
        char += String(敵キャラ[num].Lv) + "'";
        char += String(敵キャラ[num].HP) + "'";
        char += String(敵キャラ[num].MP) + "'";
        char += String(敵キャラ[num].ATK) + "'";
        char += String(敵キャラ[num].MAT) + "'";
        char += String(敵キャラ[num].DEF) + "'";
        char += String(敵キャラ[num].MDF) + "'";
        char += String(敵キャラ[num].SPD) + "'";
        char += String(敵キャラ[num].LUK) + "'";
        char += String(敵キャラ[num].EXP);
    } else if (str == "敵キャラ_ローカル") {
        char += String(敵キャラ_ローカル[num].num) + "'";
        char += String(敵キャラ_ローカル[num].img) + "'";
        char += String(敵キャラ_ローカル[num].名前) + "'";
        char += String(敵キャラ_ローカル[num].name) + "'";
        char += String(敵キャラ_ローカル[num].Lv) + "'";
        char += String(敵キャラ_ローカル[num].HP) + "'";
        char += String(敵キャラ_ローカル[num].MP) + "'";
        char += String(敵キャラ_ローカル[num].ATK) + "'";
        char += String(敵キャラ_ローカル[num].MAT) + "'";
        char += String(敵キャラ_ローカル[num].DEF) + "'";
        char += String(敵キャラ_ローカル[num].MDF) + "'";
        char += String(敵キャラ_ローカル[num].SPD) + "'";
        char += String(敵キャラ_ローカル[num].LUK) + "'";
        char += String(敵キャラ_ローカル[num].EXP);
    } else return undefined;

    return char;
}

function onload() {
    初期設定();
    document.getElementById("bgm").click();

    if (localStorage.lang != undefined) 言語 = localStorage.lang;

    setInterval(keybord, 10);
    setInterval(説明, 10);
    setInterval(情報保存, 10);

    setInterval(function() {
        var mas = document.getElementById("master").value;
        document.getElementById("master_num").innerHTML = mas;
        var mas_bgm = document.getElementById("volume").value;
        var mas_se = document.getElementById("se").value;
        BGM.volume = Math.floor(mas_bgm * mas / 100) / 100;
        SE.volume = Math.floor(mas_se * mas / 100) / 100;
        document.getElementById("vol_num").innerHTML = 四捨五入(Math.floor(四捨五入(BGM.volume * 1000 / document.getElementById("master").value, 3) * 10), -1);
        document.getElementById("se_num").innerHTML = 四捨五入(Math.floor(四捨五入(SE.volume * 1000 / document.getElementById("master").value, 3) * 10), -1);
        if (document.getElementById("vol_num").innerHTML == "NaN") document.getElementById("vol_num").innerHTML = 0;
        if (document.getElementById("se_num").innerHTML == "NaN") document.getElementById("se_num").innerHTML = 0;
        var mute = Number(document.getElementById("mute").value);
        var mute_num = document.getElementById("mute_num");
        if (mute == 1) {
            var cl = "dimgray";
            BGM.muted = true;
            SE.muted = true;
            mute_num.innerHTML = "ON";

            mute_num.style.color = "red";

            document.getElementById("vol_num").style.color = cl;
            document.getElementById("se_num").style.color = cl;
            document.getElementById("master_num").style.color = cl;
        } else {
            var cl = "black";
            BGM.muted = false;
            SE.muted = false;
            mute_num.innerHTML = "OFF";

            mute_num.style.color = cl;

            document.getElementById("vol_num").style.color = cl;
            document.getElementById("se_num").style.color = cl;
            document.getElementById("master_num").style.color = cl;
        }
        音量[0] = mas;
        音量[1] = mas_bgm;
        音量[2] = mas_se;
    }, 10);

    setInterval(function() {
        var aa = 0;
        var bb = "en";
        var jp = document.getElementsByClassName("jp").length;
        var en = document.getElementsByClassName("en").length;
        if (言語 == "jp") bb = "en";
        else bb = "jp";
        while (aa < jp) {
            document.getElementsByClassName(bb)[aa].style.display = "none";
            document.getElementsByClassName(言語)[aa].style.display = "inline-block";
            aa++;
        }
        言語表示();
    }, 10);

    select_キャラ();

    setInterval(function() {
        var ck2 = document.getElementsByName("lang");
        if (ck2.item(0).checked == true) 言語 = "jp";
        else 言語 = "en";
        localStorage.lang = 言語;
    }, 10);

    setInterval(function() {
        var a = "";
        //if (user_lank < 100) a += "0";
        if (user_lank < 10) a += "0";
        a += String(user_lank);
        document.getElementById("user_lank").innerHTML = a;
    }, 10);

    var a = 0;
    while (a < dis_id.length) {
        document.getElementById(dis_id[a]).style.display = "none";
        a++;
    }

    if (表示ID != "") document.getElementById(表示ID).style.display = "block";

    //document.getElementById("user-rank").style.width = 75 + "%";

    setInterval(function() {
        var str = "";
        if (現在画面 == "select" || 現在画面 == "config") str = "block";
        else str = "none";
        if ((確認画面_画面情報 == "select" && 現在画面 == "確認画面") || (確認画面_画面情報 == "config" && 現在画面 == "確認画面")) str = "block";
        document.getElementById("btm").style.display = str;
    }, 10);

    setInterval(function() {
        var a = document.getElementsByClassName("戦闘画面_敵選択_div");
        if (敵選択_透明度[0] == false) 敵選択_透明度[1]--;
        else 敵選択_透明度[1]++;
        a[0].style.opacity = 敵選択_透明度[1] + "%";
        a[1].style.opacity = 敵選択_透明度[1] + "%";
        a[2].style.opacity = 敵選択_透明度[1] + "%";
        if (敵選択_透明度[1] < 35) 敵選択_透明度[0] = true;
        if (敵選択_透明度[1] >= 100) 敵選択_透明度[0] = false;
    }, 10);

    setInterval(function() {
        document.getElementById("coin").innerHTML = Number(コイン).toLocaleString();
    }, 10);

    var ck0 = document.getElementsByName("lang");
    var ck1 = ck0.length;
    if (言語 == "jp") ck0.item(0).checked = true;
    else ck0.item(1).checked = true;
}

function localStorage削除(str) {
    if (str == "all") {
        var a = 0;
        while (a < LoSt.length) {
            localStorage.removeItem(LoSt[a]);
            a++;
        }
    } else {
        localStorage.removeItem(str);
    }
}

function 言語表示() {
    var str = [
        [
            "セーブデータ引継ぎ",
            "キャッシュ削除"
        ],
        [
            "Save&nbsp;data&nbsp;transfer",
            "Delete&nbsp;cache"
        ]
    ];
    var num = 0;
    if (言語 == "jp") num = 0;
    else num = 1;

    document.getElementById("引継ぎボタン").innerHTML = str[num][0];
    document.getElementById("キャッシュ削除").innerHTML = str[num][1];
}

function 四捨五入(num, num2) {
    if (num2 < 0) {
        num = Math.floor(num);
        num2 *= -1;
        num /= Math.pow(10, num2);
        num = Math.round(num);
        num *= Math.pow(10, num2);
    } else {
        num *= Math.pow(10, num2);
        num = Math.floor(num);
        num /= 10;
        num = Math.round(num);
        num /= Math.pow(10, num2 - 1);
    }
    return num;
}

function run(max, min) {
    return Math.floor(Math.random() * (max + 1 - min)) + min;
}

function stage_test(num) {
    alert(num);
}

function 起動ロード() {
    var a = document.getElementsByClassName("難易度");
    var b = 0;
    var c = "&emsp;【" + 難易度 + "】";
    if (難易度 == "NORMAL") c = "";
    while (b < a.length) {
        a[b].innerHTML = c;
        if (難易度 == "EASY") a[b].style.color = "green";
        else if (難易度 == "HARD") a[b].style.color = "red";
        else a[b].style.color = "purple";
        b++;
    }
    document.getElementById("title1").style.display = "none";
    document.getElementById("title2").style.display = "block";
    var a = 1;
    var b = 1;
    var num = 起動ロード時間;
    var cnt = Math.floor(num / 3);
    setTimeout(function() {
        document.getElementById("title_loading").innerHTML = ".&nbsp;&nbsp;";
        document.getElementById("load_bar").style.width = 33 + "%";
    }, (num * 1000 / 4));
    setTimeout(function() {
        document.getElementById("title_loading").innerHTML = "..&nbsp;";
        document.getElementById("load_bar").style.width = 66 + "%";
    }, (num * 1000 / 4 * 2));
    setTimeout(function() {
        document.getElementById("title_loading").innerHTML = "...";
        document.getElementById("load_bar").style.width = 99 + "%";
    }, (num * 1000 / 4 * 3));
    setTimeout(function() {
        document.getElementById("load_bar").style.width = 100 + "%";
    }, (num * 1000 / 4 * 3.5));

    setTimeout(function() {
        document.getElementById("title").style.display = "none";
        document.getElementById("select").style.display = "block";
        document.getElementById('title1').style.display = 'block';
        document.getElementById('title2').style.display = 'none';
        document.getElementById("btm").style.display = "block";
        現在画面 = "select";
        bgm("select");
    }, (num * 1000));

}

function 全角半角英数字変換(str) {
    return str.replace(/[A-Za-z0-9]/g, function(s) {
        return String.fromCharCode(s.charCodeAt(0) + 0xFEE0);
    });
}

setInterval(function() { //時刻
    var nowTime = new Date();
    var nowHour = nowTime.getHours();
    var nowMin = nowTime.getMinutes();
    var nowSec = nowTime.getSeconds();
    var str = "";
    if (nowHour < 10) nowHour = "0" + nowHour;
    if (nowMin < 10) nowMin = "0" + nowMin;
    if ((Number(nowSec) % 2) == 0) str = "：";
    else str = "　";
    document.getElementById("now_time").innerHTML = nowHour + str + nowMin;
}, 100);

function 全非表示() {
    var a = 0;
    while (a < dis_id.length) {
        document.getElementById(dis_id[a]).style.display = 'none';
        a++;
    }
}

function bgm(str) {
    BGM.pause();
    switch (str) {
        case "title":
            BGM.src = "bgm/title/title.mp3";
            break;

        case "select":
            BGM.src = "bgm/select/0001.mp3";
            break;

        case "config":
            BGM.src = "bgm/select/0001.mp3";
            break;

        case "編成":
            BGM.src = "bgm/select/0001.mp3";
            break;

        case "battle":
            BGM.src = "bgm/battle/0001.mp3";
            if (選択ステージ == 5) BGM.src = "bgm/battle/0002.mp3";
            break;

        case "victory":
            BGM.src = "bgm/battle/victory.mp3";
            break;

        default:
            str = "停止";
            break;
    }
    if (str != "停止") {
        BGM.loop = "true";
        BGM.play();
    }
}

function btm_タイトルに戻る() {
    var b = ["タイトルに戻りますか？", "Do you want to go back to the title?"];
    var c = 0;
    if (言語 == "en") c = 1;
    確認画面("btm_タイトルに戻る", b[c]);
}

function btm_ステージ() {
    select_キャラ();
    全非表示();
    document.getElementById("select").style.display = "block";
    document.getElementById("btm").style.display = "block";
    if (現在画面 != "select" && 現在画面 != "config" && 現在画面 != "編成") bgm("select");
    現在画面 = "select";

    var a = 0;
    while (a < 敵キャラ.length) {
        敵キャラ_ローカル[a].num = 敵キャラ[a].num;
        敵キャラ_ローカル[a].img = 敵キャラ[a].img;
        敵キャラ_ローカル[a].名前 = 敵キャラ[a].名前;
        敵キャラ_ローカル[a].name = 敵キャラ[a].name;
        敵キャラ_ローカル[a].Lv = 敵キャラ[a].Lv;
        敵キャラ_ローカル[a].HP = 敵キャラ[a].HP;
        敵キャラ_ローカル[a].MP = 敵キャラ[a].MP;
        敵キャラ_ローカル[a].ATK = 敵キャラ[a].ATK;
        敵キャラ_ローカル[a].MAT = 敵キャラ[a].MAT;
        敵キャラ_ローカル[a].DEF = 敵キャラ[a].DEF;
        敵キャラ_ローカル[a].MDF = 敵キャラ[a].MDF;
        敵キャラ_ローカル[a].SPD = 敵キャラ[a].SPD;
        敵キャラ_ローカル[a].LUK = 敵キャラ[a].LUK;
        敵キャラ_ローカル[a].EXP = 敵キャラ[a].EXP;
        a++;
    }
}

function btm_コンフィグ() {
    全非表示();
    document.getElementById("config").style.display = "block";
    document.getElementById("btm").style.display = "block";
    if (現在画面 != "config" && 現在画面 != "select") bgm("config");
    現在画面 = "config";
}

function btm_再読み込み() {
    var str = [
        "キャッシュを削除しますか？",
        "Do you want to delete the cache?"
    ];
    var num = 0;
    if (言語 == "jp") num = 0;
    else num = 1;
    確認画面("btm_再読み込み", str[num]);
}

function select_キャラ() {
    while (select_キャラ_a == select_キャラ_b) {
        select_キャラ_a = run(9, 1);
    }
    document.getElementById("select_left_char").style.backgroundImage = "url(img/char/player/char" + select_キャラ_a + ".png)";
    select_キャラ_b = select_キャラ_a;
}

function 選択背景(num) {
    ステージ選択_コントローラー選択_num = num;
    select_背景(ステージ選択_コントローラー選択_num);
}

function cmd(str) {
    switch (str) {
        case "admin":
            var input = window.prompt("password?", "");
            if (input == "200311100314") {
                全非表示();
                document.getElementById("admin").style.display = "block";
            }
            break;

        case "display":
            var input = window.prompt("id?", "");
            var a = 0;
            if (input != "admin") {
                while (a < dis_id.length) {
                    if (dis_id[a] == input) {
                        全非表示();
                        document.getElementById(input).style.display = "block";
                        表示ID = input;
                        BGM.pause();
                        bgm(input);
                    }
                    a++;
                }
            }
            break;

        case "lang":
            var input = window.prompt("language?", "");
            switch (input) {
                case "jp":
                    document.getElementById("jpn").click();
                    document.getElementById("ad_lan").innerHTML = "日本語";
                    言語 = "jp";
                    break;

                case "日本語":
                    document.getElementById("jpn").click();
                    document.getElementById("ad_lan").innerHTML = "日本語";
                    言語 = "jp";
                    break;

                case "en":
                    document.getElementById("eng").click();
                    document.getElementById("ad_lan").innerHTML = "English";
                    言語 = "en";
                    break;

                case "英語":
                    document.getElementById("eng").click();
                    document.getElementById("ad_lan").innerHTML = "English";
                    言語 = "en";
                    break;
            }
            break;

        case "play":
            bgm(表示ID);
            break;

        case "stop":
            BGM.pause();
            break;

        case "pause":
            BGM.pause();
            break;

        case "reload":
            location.reload(true);
            break;

        default:
            break;
    }
}

function admin() {}

function スキップ() {
    var skip = Number(document.getElementById("skip").innerHTML);
    var a = skip * 1000 * 選択ステージ;
    var b = コイン - a;
    if (b >= 0) {
        //スキップ可能なら
        var c = Math.floor(a * 0.38);
        var 減るもの = String(コイン.toLocaleString() + "&emsp;→&emsp;" + b.toLocaleString() + "&nbsp;(-" + a.toLocaleString() + ")&nbsp;+&nbsp;" + c.toLocaleString());
        var b = ["ステージをスキップしますか？<br><br>coin : " + 減るもの, "Do you want to skip a stage?<br><br>coin : " + 減るもの];
        var c = 0;
        if (言語 == "en") c = 1;
        確認画面("ステージをスキップしますか？", b[c]);
    } else se("戻る");
}

function ユーザーランク_EXP(num) {
    var a = document.getElementById("ユーザーランク_EXP");
    var aa = document.getElementById("user-rank");
    if (num == 1) {
        var str = "";
        var b = aa.value;
        b *= 100;
        b = Math.floor(b);
        b /= 100;
        str += (b + "% / ");
        str += user_lank_EXP.toLocaleString();
        a.style.color = "black";
        a.innerHTML = str;
    } else {
        a.style.color = "#FFFFFF00";
    }
}


function 確認画面(str1, str2) {
    確認画面_画面情報 = 現在画面;
    現在画面 = "確認画面";
    確認画面_処理判別 = str1;
    document.getElementById("確認画面").style.display = "block";
    document.getElementById("question").innerHTML = str2;
}

function 確認画面_選択(str) {
    if (str == true) {
        se('決定');
        switch (確認画面_処理判別) {
            case "btm_タイトルに戻る":
                現在画面 = "title";
                難易度 = "NORMAL";
                ステージ選択_コントローラー選択_num = -1;
                select_背景(1);
                ステージ選択_コントローラー選択(1);
                document.getElementById("mute").value = 0;
                ミュート_cnt = 0;
                全非表示();
                document.getElementById('title').style.display = 'block';
                document.getElementById("btm").style.display = "none";
                document.getElementById('load_bar').style.width = '0%';
                setTimeout(function() {
                    bgm("title");
                }, 250);
                確認画面_画面情報 = 現在画面;
                break;

            case "btm_再読み込み":
                setTimeout(function() {
                    location.reload(true);
                }, 1000);
                break;

            case "逃げる":
                全非表示();
                setTimeout(function() {
                    btm_ステージ();
                }, 100);
                確認画面_画面情報 = "select";
                bgm(確認画面_画面情報);
                break;

            case "ステージをスキップしますか？":
                var skip = Number(document.getElementById("skip").innerHTML);
                var a = skip * 1000 * 選択ステージ;
                var b = コイン - a;
                var c = Math.floor(a * 0.38);
                b += c;
                コイン = b;
                var d = [2567, 4367, 6317, 7855, 115820];
                user_lank_EXP += (d[選択ステージ - 1] * skip) * 50;

                var level = [];
                level[0] = 200;
                level[1] = 0;
                var a = 2;
                while (a <= 100) {
                    level[a] = Math.floor(level[0] * a ** 3);
                    a++;
                }

                var b2 = 99;
                while (b2 > 0) {
                    if (level[b2] <= user_lank_EXP) break;
                    b2--;
                }
                user_lank = b2;
                document.getElementById("user_lank").innerHTML = user_lank;
                if (user_lank_EXP < level[99]) document.getElementById("user-rank").value = (user_lank_EXP / level[user_lank + 1]) * 100;
                else {
                    document.getElementById("user-rank").max = 100;
                    document.getElementById("user-rank").value = 100;
                }
                se("決定");
                確認画面_画面情報 = "select";
                btm_ステージ();
                break;

            default:
                break;
        }
    } else {
        se("戻る");
    }
    document.getElementById("確認画面").style.display = "none";
    確認画面_処理判別 = "";
    現在画面 = 確認画面_画面情報;
}