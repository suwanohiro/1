function ソート(num, str) {
    var a = [...num];
    var b = 0;
    if (str == "昇順") b = ソート1(a);
    if (str == "降順") b = ソート2(a);
    return b;
}

function ソート1(num) {
    var a = [...num];
    return a.sort(ソート3);
}

function ソート2(num) {
    var a = [...num];
    return a.sort(ソート4);
}

function ソート3(a, b) {
    return a - b;
}

function ソート4(a, b) {
    return b - a;
}

function 配列要素表示(num) {
    var a = [...num];
    var b = 0;
    var str = "";
    while (b < a.length) {
        str += "a[" + b + "] : " + a[b] + "\n";
        b++;
    }
    return str;
}