var スキップ_長押し = false;

function 編成画面へ(num) {
    全非表示();
    se("決定");
    document.getElementById("skip").innerHTML = 1;
    ステージ = num;
    document.getElementById("編成").style.display = "block";
    if (現在画面 != "select" && 現在画面 != "config") bgm("編成");
    var a = 0;
    var str = "";
    while (a < 5) {
        if (a < num) str += "★";
        else str += "☆";
        a++;
    }
    document.getElementById("編成_難易度").innerHTML = str;
    var b = document.getElementsByClassName("ステージ数").length;
    if (num < 10) num = "0" + num;
    a = 0;
    while (a < b) {
        document.getElementsByClassName("ステージ数")[a].innerHTML = num;
        a++;
    }
    選択ステージ = Number(num);
    現在画面 = "編成";
}

var 編成1_a = -1;
var 編成1_b = 0;
var 編成1_old = 0;

function 編成_選択解除() {
    var str = "2px solid black";
    document.getElementById("編成1").style.border = str;
    document.getElementById("編成2").style.border = str;
    document.getElementById("編成3").style.border = str;
}

function 編成(num) {
    if (編成1_old == num || 編成1_old == 0) {
        編成1_b++;
        if (編成1_b % 2 == 1) {
            編成1_a = num;
            編成_選択解除();
            document.getElementById("編成" + num).style.border = "2px solid red";
            編成1_old = num;
        } else {
            if (編成1_old == num) {
                編成_選択解除();
                編成中のキャラ[num - 1] = 0;
                se("戻る");
                document.getElementById("編成" + 編成1_a).style.backgroundImage = "";
                編成1_a = 0;
                編成1_b = 0;
                編成1_old = 0;
            }
        }
    }
}

function 編成_選択(num) {
    var a = 編成1_a;
    if (編成1_a != 0 && 編成1_a != -1) {
        var c = -1;
        if (編成中のキャラ[0] == num) c = 0;
        if (編成中のキャラ[1] == num) c = 1;
        if (編成中のキャラ[2] == num) c = 2;
        if (c == -1) {
            document.getElementById("編成" + 編成1_a).style.backgroundImage = "url(img/char/player/char" + num + ".png)";
            編成_選択解除();
            編成中のキャラ[a - 1] = num;
        } else {
            //alert("同じキャラは編成できません");
            se("戻る");
            編成_選択解除();
        }
        編成1_a = 0;
        編成1_b = 0;
        編成1_old = 0;
    }
}

function ステータス値表示(num) {
    if (num != 0) {
        var a = ["名前", "Lv", "HP", "MP", "ATK", "DEF", "MAT", "MDF", "SPD", "LUK"];
        var b = キャラ[num].EXP;
        if (言語 == "jp") document.getElementById("編成_名前").innerHTML = キャラ[num].名前;
        else document.getElementById("編成_名前").innerHTML = キャラ[num].name;
        if (num != 0) document.getElementById("編成_Lv").innerHTML = キャラ[num].Lv + "<span style='font-size: 75%'>&nbsp(EXP：" + b.toLocaleString() + ")</span>";
        else document.getElementById("編成_Lv").innerHTML = キャラ[num].Lv;
        document.getElementById("編成_HP").innerHTML = (キャラ[num].HP).toLocaleString();
        document.getElementById("編成_MP").innerHTML = (キャラ[num].MP).toLocaleString();
        document.getElementById("編成_ATK").innerHTML = (キャラ[num].ATK).toLocaleString();
        document.getElementById("編成_DEF").innerHTML = (キャラ[num].DEF).toLocaleString();
        document.getElementById("編成_MAT").innerHTML = (キャラ[num].MAT).toLocaleString();
        document.getElementById("編成_MDF").innerHTML = (キャラ[num].MDF).toLocaleString();
        document.getElementById("編成_SPD").innerHTML = (キャラ[num].SPD).toLocaleString();
        document.getElementById("編成_LUK").innerHTML = (キャラ[num].LUK).toLocaleString();
    } else {
        document.getElementById("編成_名前").innerHTML = "";
        document.getElementById("編成_Lv").innerHTML = "";
        document.getElementById("編成_HP").innerHTML = "";
        document.getElementById("編成_MP").innerHTML = "";
        document.getElementById("編成_ATK").innerHTML = "";
        document.getElementById("編成_DEF").innerHTML = "";
        document.getElementById("編成_MAT").innerHTML = "";
        document.getElementById("編成_MDF").innerHTML = "";
        document.getElementById("編成_SPD").innerHTML = "";
        document.getElementById("編成_LUK").innerHTML = "";
    }
}

function カウント(str) {
    スキップ_長押し = true;
    var a = document.getElementById("skip");
    if (str == "+") {
        var b = Number(a.innerHTML);
        b++;
        if (b < 1000) a.innerHTML = b;
    }
    if (str == "-") {
        var b = Number(a.innerHTML);
        b--;
        if (b > 0) a.innerHTML = b;
    }
    setTimeout(function() {
        if (スキップ_長押し == true) カウント(str);
    }, 100);
}

function カウント2(str) {
    var a = document.getElementById("skip");
    var z = 0;
    if (str == "+10") {
        str = "+";
        z = 9;
    }
    if (str == "-10") {
        str = "-";
        z = -9;
    }
    if (str == "+") {
        var b = Number(a.innerHTML);
        b += (1 + z);
        if (b < 1000) a.innerHTML = b;
    }
    if (str == "-") {
        var b = Number(a.innerHTML);
        b -= (1 + z);
        if (b > 0) a.innerHTML = b;
    }
}

const check_sec = 500; //ミリ秒
const target_element = document.getElementById('bt-prac');

long_press(target_element, normal_func, long_func, check_sec);

function normal_func() {
    スキップ_長押し = false;
}

function long_func() {
    スキップ_長押し = true;
}