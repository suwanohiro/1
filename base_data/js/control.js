var 操作説明 = [{
        set: "",
        A: "",
        B: "",
        X: "",
        Y: "",
        L: "",
        R: "",
        ZL: "",
        ZR: "",
        up: "",
        down: "",
        left: "",
        right: "",
        L_stick: "",
        R_stick: "",
        L_push: "",
        R_push: "",
        start: "",
        select: ""
    },
    {
        set: "title",
        A_jp: "ゲームスタート",
        B_jp: "",
        X_jp: "",
        Y_jp: "",
        L_jp: "",
        R_jp: "",
        ZL_jp: "",
        ZR_jp: "",
        up_jp: "",
        down_jp: "",
        left_jp: "",
        right_jp: "",
        L_stick_jp: "",
        R_stick_jp: "",
        L_push_jp: "",
        R_push_jp: "",
        start_jp: "",
        select_jp: "",
        A_en: "Game Start",
        B_en: "",
        X_en: "",
        Y_en: "",
        L_en: "",
        R_en: "",
        ZL_en: "",
        ZR_en: "",
        up_en: "",
        down_en: "",
        left_en: "",
        right_en: "",
        L_stick_en: "",
        R_stick_en: "",
        L_push_en: "",
        R_push_en: "",
        start_en: "",
        select_en: ""
    },
    {
        set: "select",
        A_jp: "ゲームスタート",
        B_jp: "",
        X_jp: "",
        Y_jp: "",
        L_jp: "",
        R_jp: "",
        ZL_jp: "",
        ZR_jp: "",
        up_jp: "",
        down_jp: "",
        left_jp: "",
        right_jp: "",
        L_stick_jp: "",
        R_stick_jp: "",
        L_push_jp: "",
        R_push_jp: "",
        start_jp: "",
        select_jp: "",
        A_en: "Game Start",
        B_en: "",
        X_en: "",
        Y_en: "",
        L_en: "",
        R_en: "",
        ZL_en: "",
        ZR_en: "",
        up_en: "",
        down_en: "",
        left_en: "",
        right_en: "",
        L_stick_en: "",
        R_stick_en: "",
        L_push_en: "",
        R_push_en: "",
        start_en: "",
        select_en: ""
    }
];

var 説明画面表示_cnt = 0;

function 説明画面表示() {
    var a = document.getElementById("操作説明");
    説明画面表示_cnt++;
    if (説明画面表示_cnt % 2 == 1) {
        a.style.display = "block";
    } else a.style.display = "none";

    var a = 0;
    var b = [];
    while ((a + 1) < 操作説明.length) {
        b[a] = 操作説明[a + 1].set;
        a++;
    }
    var c = b.indexOf(現在画面);

    if (説明画面表示_cnt % 2 == 1) {
        操作説明_画面情報 = 現在画面;
        現在画面 = "操作説明";
    } else {
        現在画面 = 操作説明_画面情報;
    }
}

function 説明() {
    説明_表示(1);
    switch (現在画面) {
        case "title":
            break;

        default:
            break;
    }
}

function 説明_表示(num) {
    var a = document.getElementsByClassName("操作説明");
    if (言語 == "jp") {
        document.getElementsByClassName("操作説明_タイトル")[0].innerHTML = "操作説明";
        document.getElementsByClassName("操作説明_タイトル")[1].innerHTML = "この画面を閉じる";
        a[0].innerHTML = 操作説明[num].A_jp;
        a[3].innerHTML = 操作説明[num].B_jp;
        a[6].innerHTML = 操作説明[num].X_jp;
        a[7].innerHTML = 操作説明[num].Y_jp;
        a[1].innerHTML = 操作説明[num].L_jp;
        a[2].innerHTML = 操作説明[num].R_jp;
        a[4].innerHTML = 操作説明[num].ZL_jp;
        a[5].innerHTML = 操作説明[num].ZR_jp;
        a[8].innerHTML = 操作説明[num].up_jp;
        a[10].innerHTML = 操作説明[num].down_jp;
        a[12].innerHTML = 操作説明[num].left_jp;
        a[15].innerHTML = 操作説明[num].right_jp;
        a[9].innerHTML = 操作説明[num].L_stick_jp;
        a[11].innerHTML = 操作説明[num].R_stick_jp;
        a[13].innerHTML = 操作説明[num].L_push_jp;
        a[16].innerHTML = 操作説明[num].R_push_jp;
        a[14].innerHTML = 操作説明[num].start_jp;
        a[17].innerHTML = 操作説明[num].select_jp;
    } else {
        document.getElementsByClassName("操作説明_タイトル")[0].innerHTML = "Operation&nbsp;Description";
        document.getElementsByClassName("操作説明_タイトル")[1].innerHTML = "Close&nbsp;this&nbsp;screen";
        a[0].innerHTML = 操作説明[num].A_en;
        a[3].innerHTML = 操作説明[num].B_en;
        a[6].innerHTML = 操作説明[num].X_en;
        a[7].innerHTML = 操作説明[num].Y_en;
        a[1].innerHTML = 操作説明[num].L_en;
        a[2].innerHTML = 操作説明[num].R_en;
        a[4].innerHTML = 操作説明[num].ZL_en;
        a[5].innerHTML = 操作説明[num].ZR_en;
        a[8].innerHTML = 操作説明[num].up_en;
        a[10].innerHTML = 操作説明[num].down_en;
        a[12].innerHTML = 操作説明[num].left_en;
        a[15].innerHTML = 操作説明[num].right_en;
        a[9].innerHTML = 操作説明[num].L_stick_en;
        a[11].innerHTML = 操作説明[num].R_stick_en;
        a[13].innerHTML = 操作説明[num].L_push_en;
        a[16].innerHTML = 操作説明[num].R_push_en;
        a[14].innerHTML = 操作説明[num].start_en;
        a[17].innerHTML = 操作説明[num].select_en;
    }
}