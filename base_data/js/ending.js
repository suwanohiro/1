var ending_bgm = new Audio();
var ending_flg = false;
var ending_end = false;

function ending() {
    ending_end = false;
    ending_flg = true;
    document.getElementById("movie").animate([{ opacity: '0' }, { opacity: '1' }], 2000);
    //document.getElementById("ending_movie").play();

    setTimeout(function() {
        ending_bgm.volume = BGM.volume;
    }, 25);

    ending_bgm.pause();
    ending_bgm.src = "bgm/ending/ending.mp3";
    ending_bgm.play();
    ending_bgm.playbackRate = 10;
    ending_loop();

    setTimeout(function() {
        var a = document.getElementById("asd256");
        var b = document.createElement("div");
        b.className = "fade";
        b.innerHTML = "test";
        b.style.fontSize = "50vh";
        //a.appendChild(b);
    }, 2000);
}

function ending_loop() {
    if (ending_bgm.currentTime > 100) ending_bgm.playbackRate = 1;
    document.title = Math.floor(ending_bgm.currentTime) + " / " + Math.floor(ending_bgm.duration);
    if (ending_bgm.currentTime > (ending_bgm.duration - 2) && !ending_end) {
        document.getElementById("movie").animate([{ opacity: '1' }, { opacity: '0' }], 2000);
        ending_end = true;
    }
    if (ending_bgm.currentTime == ending_bgm.duration && 現在画面 != "select") {
        ending_flg = false;
        bgm("select");
        btm_ステージ();
    }

    setTimeout(function() {
        if (ending_flg) ending_loop();
    }, 1);
}