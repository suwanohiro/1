var キャラ = [{
        num: 0,
        img: "",
        名前: "",
        name: "",
        Lv: "",
        HP: "",
        MP: "",
        ATK: "",
        MAT: "",
        DEF: "",
        MDF: "",
        SPD: "",
        LUK: "",
        EXP: ""
    },
    {
        num: 1,
        img: "img/char/player/char1.png",
        名前: "坂本",
        name: "Sakamoto",
        Lv: 1,
        HP: 397,
        MP: 5,
        ATK: 182,
        MAT: 5,
        DEF: 76,
        MDF: 53,
        SPD: 126,
        LUK: 238,
        EXP: 0
    },
    {
        num: 2,
        img: "img/char/player/char2.png",
        名前: "ありす",
        name: "Alice",
        Lv: 0,
        HP: 291,
        MP: 307,
        ATK: 54,
        MAT: 238,
        DEF: 53,
        MDF: 79,
        SPD: 114,
        LUK: 126,
        EXP: 0
    },
    {
        num: 3,
        img: "img/char/player/char3.png",
        名前: "アレス",
        name: "Ares",
        Lv: 1,
        HP: 425,
        MP: 195,
        ATK: 173,
        MAT: 168,
        DEF: 72,
        MDF: 68,
        SPD: 116,
        LUK: 140,
        EXP: 0
    },
    {
        num: 4,
        img: "img/char/player/char4.png",
        名前: "アミー",
        name: "Amie",
        Lv: 1,
        HP: 429,
        MP: 146,
        ATK: 159,
        MAT: 160,
        DEF: 68,
        MDF: 88,
        SPD: 111,
        LUK: 157,
        EXP: 0
    },
    {
        num: 5,
        img: "img/char/player/char5.png",
        名前: "ナハス",
        name: "Nahas",
        Lv: 0,
        HP: 539,
        MP: 162,
        ATK: 171,
        MAT: 43,
        DEF: 147,
        MDF: 53,
        SPD: 115,
        LUK: 128,
        EXP: 0
    },
    {
        num: 6,
        img: "img/char/player/char6.png",
        名前: "エア",
        name: "Air",
        Lv: 0,
        HP: 328,
        MP: 274,
        ATK: 52,
        MAT: 206,
        DEF: 46,
        MDF: 71,
        SPD: 114,
        LUK: 119,
        EXP: 0
    },
    {
        num: 7,
        img: "img/char/player/char7.png",
        名前: "ネイト",
        name: "Nate",
        Lv: 0,
        HP: 394,
        MP: 216,
        ATK: 163,
        MAT: 153,
        DEF: 56,
        MDF: 71,
        SPD: 114,
        LUK: 138,
        EXP: 0
    },
    {
        num: 8,
        img: "img/char/player/char8.png",
        名前: "ディアーナ",
        name: "Dianna",
        Lv: 1,
        HP: 382,
        MP: 167,
        ATK: 168,
        MAT: 96,
        DEF: 53,
        MDF: 63,
        SPD: 108,
        LUK: 126,
        EXP: 0
    },
    {
        num: 9,
        img: "img/char/player/char9.png",
        名前: "ヴァルター",
        name: "Valter",
        Lv: 1,
        HP: 416,
        MP: 118,
        ATK: 255,
        MAT: 107,
        DEF: 79,
        MDF: 42,
        SPD: 76,
        LUK: 156,
        EXP: 0
    }
];

var キャラ_初期値 = [{
        num: 0,
        img: "",
        名前: "",
        name: "",
        Lv: "",
        HP: "",
        MP: "",
        ATK: "",
        MAT: "",
        DEF: "",
        MDF: "",
        SPD: "",
        LUK: "",
        EXP: ""
    },
    {
        num: 1,
        img: "img/char/player/char1.png",
        名前: "坂本",
        name: "Sakamoto",
        Lv: 1,
        HP: 397,
        MP: 5,
        ATK: 182,
        MAT: 5,
        DEF: 76,
        MDF: 53,
        SPD: 126,
        LUK: 238,
        EXP: 0
    },
    {
        num: 2,
        img: "img/char/player/char2.png",
        名前: "ありす",
        name: "Alice",
        Lv: 1,
        HP: 291,
        MP: 307,
        ATK: 104,
        MAT: 238,
        DEF: 53,
        MDF: 79,
        SPD: 114,
        LUK: 126,
        EXP: 0
    },
    {
        num: 3,
        img: "img/char/player/char3.png",
        名前: "アレス",
        name: "Ares",
        Lv: 1,
        HP: 425,
        MP: 195,
        ATK: 173,
        MAT: 168,
        DEF: 72,
        MDF: 68,
        SPD: 116,
        LUK: 140,
        EXP: 0
    },
    {
        num: 4,
        img: "img/char/player/char4.png",
        名前: "アミー",
        name: "Amie",
        Lv: 1,
        HP: 429,
        MP: 146,
        ATK: 159,
        MAT: 160,
        DEF: 68,
        MDF: 88,
        SPD: 111,
        LUK: 157,
        EXP: 0
    },
    {
        num: 5,
        img: "img/char/player/char5.png",
        名前: "ナハス",
        name: "Nahas",
        Lv: 1,
        HP: 539,
        MP: 162,
        ATK: 171,
        MAT: 147,
        DEF: 43,
        MDF: 53,
        SPD: 115,
        LUK: 128,
        EXP: 0
    },
    {
        num: 6,
        img: "img/char/player/char6.png",
        名前: "エア",
        name: "Air",
        Lv: 1,
        HP: 328,
        MP: 274,
        ATK: 92,
        MAT: 206,
        DEF: 46,
        MDF: 71,
        SPD: 114,
        LUK: 119,
        EXP: 0
    },
    {
        num: 7,
        img: "img/char/player/char7.png",
        名前: "ネイト",
        name: "Nate",
        Lv: 1,
        HP: 394,
        MP: 216,
        ATK: 163,
        MAT: 153,
        DEF: 56,
        MDF: 71,
        SPD: 114,
        LUK: 138,
        EXP: 0
    },
    {
        num: 8,
        img: "img/char/player/char8.png",
        名前: "ディアーナ",
        name: "Dianna",
        Lv: 1,
        HP: 382,
        MP: 167,
        ATK: 168,
        MAT: 96,
        DEF: 53,
        MDF: 63,
        SPD: 108,
        LUK: 126,
        EXP: 0
    },
    {
        num: 9,
        img: "img/char/player/char9.png",
        名前: "ヴァルター",
        name: "Valter",
        Lv: 1,
        HP: 416,
        MP: 118,
        ATK: 205,
        MAT: 127,
        DEF: 79,
        MDF: 42,
        SPD: 76,
        LUK: 156,
        EXP: 0
    }
];

var キャラ_戦闘 = [{
    Player: "1P",
    名前: "",
    HP: 0,
    MP: 0,
    Lv: 0,
    ATK: 0,
    MAT: 0,
    SPD: 0
}, {
    Player: "2P",
    名前: "",
    HP: 0,
    MP: 0,
    Lv: 0,
    ATK: 0,
    MAT: 0,
    SPD: 0
}, {
    Player: "3P",
    名前: "",
    HP: 0,
    MP: 0,
    Lv: 0,
    ATK: 0,
    MAT: 0,
    SPD: 0
}, {
    Player: "1P_base",
    名前: "",
    HP: 0,
    MP: 0,
    Lv: 0,
    ATK: 0,
    MAT: 0,
    SPD: 0
}, {
    Player: "2P_base",
    名前: "",
    HP: 0,
    MP: 0,
    Lv: 0,
    ATK: 0,
    MAT: 0,
    SPD: 0
}, {
    Player: "3P_base",
    名前: "",
    HP: 0,
    MP: 0,
    Lv: 0,
    ATK: 0,
    MAT: 0,
    SPD: 0
}];

var 敵配置 = [
    [
        "ステージ0",
        1,
        2,
        3,
        1,
        1,
        1
    ],
    [
        "ステージ1",
        1,
        2,
        3,
        8,
        5,
        6
    ],
    [
        "ステージ2",
        3,
        2,
        1,
        15,
        18,
        17
    ],
    [
        "ステージ3",
        1,
        2,
        3,
        28,
        35,
        22
    ],
    [
        "ステージ4",
        1,
        2,
        3,
        40,
        32,
        38
    ],
    [
        "ステージ5",
        0,
        5,
        0,
        1,
        1,
        1
    ],
];

var 敵キャラ = [{
        num: 0,
        img: "",
        名前: "",
        name: "",
        Lv: "",
        HP: "",
        MP: "",
        ATK: "",
        MAT: "",
        DEF: "",
        MDF: "",
        SPD: "",
        LUK: "",
        EXP: ""
    },
    {
        num: 1,
        img: "/school/kadai/img/char/mob/000001.png",
        名前: "ヤマト",
        name: "Yamato",
        Lv: 1,
        HP: 63,
        MP: 45,
        ATK: 51 * 2,
        MAT: 26 * 2,
        DEF: 43,
        MDF: 32,
        SPD: 134,
        LUK: 46,
        EXP: 0
    },
    {
        num: 2,
        img: "https://lh3.googleusercontent.com/pw/AM-JKLVUIoW9CKalBeOvacLISgfglLo-mS0etDbRxk-LSvM39biots7TVnOC_N7dzUxD0uoQDstN1Bp6okL2RHt8hNWbPGUtYB9P_Gifo8bxX14esDb_Irtr3aLjPah6szJCpMffKwIDdMoBJZv971iYpRy89A=s795-no",
        名前: "おはなす",
        name: "Ohanasu",
        Lv: 1,
        HP: 85,
        MP: 114,
        ATK: 68 * 2,
        MAT: 95 * 2,
        DEF: 61,
        MDF: 86,
        SPD: 74,
        LUK: 67,
        EXP: 0
    },
    {
        num: 3,
        img: "/school/kadai/img/char/mob/000003.png",
        名前: "イッヌ",
        name: "dog",
        Lv: 1,
        HP: 113,
        MP: 36,
        ATK: 127 * 2,
        MAT: 29 * 2,
        DEF: 25,
        MDF: 56,
        SPD: 113,
        LUK: 35,
        EXP: 0
    },
    {
        num: 4,
        img: "/school/kadai/img/char/mob/000006.png",
        名前: "オオクチノマガミ",
        name: "wild dog",
        Lv: 1,
        HP: 113 ** 3.5,
        MP: 36 ** 4.5,
        ATK: 127 ** 2,
        MAT: 29 ** 2,
        DEF: 25 ** 5.5,
        MDF: 56 ** 3.5,
        SPD: 113 ** 0.5,
        LUK: 35 ** 0.5,
        EXP: 0
    },
    {
        num: 5,
        img: "/school/kadai/img/char/mob/000005.png",
        名前: "りゅうおう",
        name: "dragon",
        Lv: 1,
        HP: 18650,
        MP: 1658,
        ATK: 1033 / 3.5,
        MAT: 1936 / 3.5,
        DEF: 761 ** 1.75,
        MDF: 1198 ** 1.75,
        SPD: 1312 / 5,
        LUK: 190,
        EXP: 0
    }
];

var 敵キャラ_ローカル = [{
        num: 0,
        img: "",
        名前: "",
        name: "",
        Lv: "",
        HP: "",
        MP: "",
        ATK: "",
        MAT: "",
        DEF: "",
        MDF: "",
        SPD: "",
        LUK: "",
        EXP: ""
    },
    {
        num: 1,
        img: "/school/kadai/img/char/mob/000001.png",
        名前: "ヤマト",
        name: "Yamato",
        Lv: 1,
        HP: 63,
        MP: 45,
        ATK: 51,
        MAT: 26,
        DEF: 43,
        MDF: 32,
        SPD: 134,
        LUK: 46,
        EXP: 0
    },
    {
        num: 2,
        img: "/school/kadai/img/char/mob/000002.png",
        名前: "おはなす",
        name: "Ohanasu",
        Lv: 1,
        HP: 85,
        MP: 114,
        ATK: 68,
        MAT: 95,
        DEF: 61,
        MDF: 86,
        SPD: 74,
        LUK: 67,
        EXP: 0
    },
    {
        num: 3,
        img: "/school/kadai/img/char/mob/000003.png",
        名前: "イッヌ",
        name: "dog",
        Lv: 1,
        HP: 310,
        MP: 36,
        ATK: 127,
        MAT: 29,
        DEF: 53,
        MDF: 113,
        SPD: 113,
        LUK: 35,
        EXP: 0
    },
    {
        num: 4,
        img: "/school/kadai/img/char/mob/000006.png",
        名前: "オオクチノマガミ",
        name: "wild dog",
        Lv: 1,
        HP: 113 * 2,
        MP: 36 * 2,
        ATK: 127 * 2,
        MAT: 29 * 2,
        DEF: 25 * 2,
        MDF: 56 * 2,
        SPD: 113 * 2,
        LUK: 35 * 2,
        EXP: 0
    },
    {
        num: 5,
        img: "https://psneolog.com/wp-content/uploads/2020/01/grate.png",
        名前: "りゅうおう",
        name: "dragon",
        Lv: 1,
        HP: 18650,
        MP: 1658,
        ATK: 1033,
        MAT: 1936,
        DEF: 761,
        MDF: 1198,
        SPD: 1312,
        LUK: 190,
        EXP: 0
    }
];

function run(max, min) {
    return Math.floor(Math.random() * (max + 1 - min)) + min;
}