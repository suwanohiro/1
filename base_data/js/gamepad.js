let connectedGamepadIndex;
let loopID;
var haveEvents = 'GamepadEvent' in window;
var controllers = {};
var rAF = window.requestAnimationFrame;
//['方向', 'ボタン']
var ボタン入力 = ["", ""];

//接続された時の処理1
function connectHandler(e) {
    connectedGamepadIndex = e.gamepad.index;
    loopID = requestAnimationFrame(loop);
    addGamepad(e.gamepad);
}

//接続された時の処理2
function addGamepad(gamepad) {
    // gamepadのArrayを作成
    controllers[gamepad.index] = gamepad;
    rAF(updateStatus);
}

//接続解除時の処理
function disconnectHandler(e) {
    connectedGamepadIndex = null;
    cancelAnimationFrame(loopID);
}

function ボタン入力_save(str, num) {
    num = Number(num);
    switch (str) {
        //十字キー
        case "pov":
            switch (num) {
                //上
                case -1:
                    ボタン入力[0] = "pov_up";
                    break;

                    //下
                case 0.1:
                    ボタン入力[0] = "pov_bottom";
                    break;

                    //右
                case -0.4:
                    ボタン入力[0] = "pov_right";
                    break;

                    //左
                case 0.7:
                    ボタン入力[0] = "pov_left";
                    break;

                default:
                    return 0;
            }
            break;

            //Lスティック(左右)
        case "L_pad_width":
            switch (num) {
                //左
                case -1:
                    ボタン入力[0] = "L_pad_left";
                    break;

                    //右
                case 1:
                    ボタン入力[0] = "L_pad_right";
                    break;

                default:
                    return 0;
            }
            break;

            //Lスティック(上下)
        case "L_pad_height":
            switch (num) {
                //上
                case -1:
                    ボタン入力[0] = "L_pad_up";
                    break;

                    //下
                case 1:
                    ボタン入力[0] = "L_pad_bottom";
                    break;

                default:
                    return 0;
            }
            break;

            //Rスティック(左右)
        case "R_pad_width":
            switch (num) {
                //左
                case -1:
                    ボタン入力[0] = "R_pad_left";
                    break;

                    //右
                case 1:
                    ボタン入力[0] = "R_pad_right";
                    break;

                default:
                    return 0;
            }
            break;

            //Rスティック(上下)
        case "R_pad_height":
            switch (num) {
                //上
                case -1:
                    ボタン入力[0] = "R_pad_up";
                    break;

                    //下
                case 1:
                    ボタン入力[0] = "R_pad_bottom";
                    break;

                default:
                    return 0;
            }
            break;

        default:
            break;
    }

    return 1;
}

//アナログスティック取得
function updateStatus() {
    ボタン入力[0] = "";
    //設定
    var gamepads = navigator.getGamepads ? navigator.getGamepads() : [];
    for (var i = 0; i < gamepads.length; i++) {
        if (gamepads[i]) {
            if (!(gamepads[i].index in controllers)) {
                addGamepad(gamepads[i]);
                console.log("a");
            } else {
                controllers[gamepads[i].index] = gamepads[i];
                //console.log("b");
            }
        }
    }
    //アナログスティック取得
    for (j in controllers) {
        var a = 0;
        var controller = controllers[j];

        //アナログコントロール情報の状態取得
        var pov = Number(四捨五入処理(controller.axes[9], 2));
        var L_pad_width = Number(四捨五入処理(controller.axes[0], 2));
        var L_pad_height = Number(四捨五入処理(controller.axes[1], 2));
        var R_pad_width = Number(四捨五入処理(controller.axes[2], 2));
        var R_pad_height = Number(四捨五入処理(controller.axes[5], 2));
        a += ボタン入力_save("L_pad_width", L_pad_width);
        a += ボタン入力_save("L_pad_height", L_pad_height);
        a += ボタン入力_save("R_pad_width", R_pad_width);
        a += ボタン入力_save("R_pad_height", R_pad_height);
        a += ボタン入力_save("pov", pov);
        //if (a == 0) ボタン入力[0] = "";
    }
    rAF(updateStatus);
}

if (haveEvents) {
    //接続状況の確認
    window.addEventListener("gamepadconnected", connectHandler);
    window.addEventListener("gamepaddisconnected", disconnectHandler);
} else {
    setInterval(scanGamepads, 500);
}

function loop(timestamp) {
    ボタン入力[1] = "";
    // ゲームパッドの入力情報を毎フレーム取得します。
    let gamepads = navigator.getGamepads();
    let gp = gamepads[connectedGamepadIndex];

    // ボタンが押されているかどうかを取得します。
    var Aボタン = gp.buttons[3];
    var Bボタン = gp.buttons[2];
    var Xボタン = gp.buttons[1];
    var Yボタン = gp.buttons[0];
    var Lボタン = gp.buttons[4];
    var Rボタン = gp.buttons[5];
    var ZLボタン = gp.buttons[6];
    var ZRボタン = gp.buttons[7];
    var SELECTボタン = gp.buttons[8];
    var STARTボタン = gp.buttons[9];

    if (Aボタン.pressed) ボタン入力[1] = "A";
    if (Bボタン.pressed) ボタン入力[1] = "B";
    if (Xボタン.pressed) ボタン入力[1] = "X";
    if (Yボタン.pressed) ボタン入力[1] = "Y";
    if (Lボタン.pressed) ボタン入力[1] = "L";
    if (Rボタン.pressed) ボタン入力[1] = "R";
    if (ZLボタン.pressed) ボタン入力[1] = "ZL";
    if (ZRボタン.pressed) ボタン入力[1] = "ZR";
    if (SELECTボタン.pressed) ボタン入力[1] = "SELECT";
    if (STARTボタン.pressed) ボタン入力[1] = "START";
    requestAnimationFrame(loop);
}

function 四捨五入処理(num, num2) {
    if (num2 < 0) {
        num = Math.floor(num);
        num2 *= -1;
        num /= Math.pow(10, num2);
        num = Math.round(num);
        num *= Math.pow(10, num2);
    } else {
        num *= Math.pow(10, num2);
        num = Math.floor(num);
        num /= 10;
        num = Math.round(num);
        num /= Math.pow(10, num2 - 1);
    }
    return num;
}