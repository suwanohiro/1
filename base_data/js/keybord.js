function keybord() {
    //alert("a");
    addEventListener("keydown", keydownfunc, false);
    addEventListener("keyup", function() {
        スキップ_長押し = false;
    }, false);
    if (現在画面 != "config" || ボタン入力[0] == "") game_pad();
    else if (現在画面 == "config") config_pad();
}

function keydownfunc(event) {
    var key_code = event.keyCode;

    switch (key_code) {
        case 13: //Enter
            startボタン();
            break;

        case 16: //L-Shift
            Lボタン();
            break;

        case 27: //Esc
            selectボタン();
            break;

        case 32:
            説明画面表示();
            break;

        case 37: //左
            左ボタン();
            break;

        case 38: //上
            上ボタン();
            break;

        case 39: //右
            右ボタン();
            break;

        case 40: //下
            下ボタン();
            break;

        case 65: //A
            Rボタン();
            break;

        case 68:
            Yボタン();
            break;

        case 69: //E
            ZRボタン();
            break;

        case 73: //I
            LS上();
            break;

        case 74: //J
            LS左();
            break;

        case 75: //K
            LS下();
            break;

        case 76: //L
            LS右();
            break;

        case 81: //Q
            ZLボタン();
            break;

        case 83: //S
            Xボタン();
            break;

        case 88: //X
            Bボタン();
            break;

        case 90: //Z
            Aボタン();
            break;

        case 100: //num_4
            RS左();
            break;

        case 101: //num_5
            RS下();
            break;

        case 102: //num_6
            RS右();
            break;

        case 104: //num_8
            RS上();
            break;

        case 191: // "/"
            var input = window.prompt("Command?", "");
            cmd(input);
            break;

        default:
            document.title = key_code;
            break;
    }
}

var game_pad_old_a = "";
var game_pad_old_b = "";

function game_pad() {
    var a = ボタン入力[0];
    var b = ボタン入力[1];
    if (game_pad_old_a != a) {
        //方向キー入力時
        switch (a) {
            case "pov_up":
                上ボタン();
                break;

            case "pov_bottom":
                下ボタン();
                break;

            case "pov_left":
                左ボタン();
                break;

            case "pov_right":
                右ボタン();
                break;

            case "L_pad_up":
                LS上();
                break;

            case "L_pad_bottom":
                LS下();
                break;

            case "L_pad_left":
                LS左();
                break;

            case "L_pad_right":
                LS右();
                break;

            case "R_pad_up":
                RS上();
                break;

            case "R_pad_bottom":
                RS下();
                break;

            case "R_pad_left":
                RS左();
                break;

            case "R_pad_right":
                RS右();
                break;

            default:
                break;
        }
    }

    if (game_pad_old_b != b) {
        //ボタン入力時
        switch (b) {
            case "A":
                Aボタン();
                break;

            case "B":
                Bボタン();
                break;

            case "X":
                Xボタン();
                break;

            case "Y":
                Yボタン();
                break;

            case "L":
                Lボタン();
                break;

            case "R":
                Rボタン();
                break;

            case "ZL":
                ZLボタン();
                break;

            case "ZR":
                ZRボタン();
                break;

            case "START":
                startボタン();
                break;

            case "SELECT":
                selectボタン();
                break;

            default:
                break;
        }
    }

    game_pad_old_a = a;
    game_pad_old_b = b;
}

function config_pad() {
    var a = ボタン入力[0];
    var b = ボタン入力[1];
    //方向キー入力時
    switch (a) {
        case "pov_up":
            上ボタン();
            break;

        case "pov_bottom":
            下ボタン();
            break;

        case "pov_left":
            左ボタン();
            break;

        case "pov_right":
            右ボタン();
            break;

        case "L_pad_up":
            LS上();
            break;

        case "L_pad_bottom":
            LS下();
            break;

        case "L_pad_left":
            LS左();
            break;

        case "L_pad_right":
            LS右();
            break;

        case "R_pad_up":
            RS上();
            break;

        case "R_pad_bottom":
            RS下();
            break;

        case "R_pad_left":
            RS左();
            break;

        case "R_pad_right":
            RS右();
            break;

        default:
            break;
    }

    //ボタン入力時
    switch (b) {
        case "A":
            Aボタン();
            break;

        case "B":
            Bボタン();
            break;

        case "X":
            Xボタン();
            break;

        case "Y":
            Yボタン();
            break;

        case "L":
            Lボタン();
            break;

        case "R":
            Rボタン();
            break;

        case "ZL":
            ZLボタン();
            break;

        case "ZR":
            ZRボタン();
            break;

        default:
            break;
    }

    game_pad_old_a = a;
    game_pad_old_b = b;
}

var bor = "0.125vw solid yellow";


var old_dis = "";
setInterval(function() {
    if (現在画面 != old_dis) {
        old_dis = 現在画面;
        user = true;
    }
    //document.title = "現在画面 : " + 現在画面;
}, 10);

var 編成_コントロール = 0;

/*setInterval(function() {
    document.getElementsByClassName("編成_コントロール")[11].style.backgroundColor = "lightblue";
}, 10);
*/

//上下キーを押したらスクロールするように組む

function ステージ選択_コントローラー選択(num) {
    //alert(0);
    var a = ステージ選択_コントローラー選択_num;
    a = num;
    var wh = window.innerHeight + 1;
    wh = wh / 720;
    var ステージ = [
        //ステージ1
        0,

        //ステージ2
        50,

        //ステージ3
        175 * wh,

        //ステージ4
        331 * wh,

        //ステージ5
        document.getElementById("ステージ選択").scrollHeight
    ];
    document.getElementById("ステージ選択").scrollTop = ステージ[num - 1];
}

function config_コントローラー選択(num) {
    var a = Number(document.getElementById("config_main").scrollTop);
    var b = Number(document.getElementById("config_main").scrollHeight);
    var c = 0;
    var d = 20;
    if (num >= 0) c = a + d;
    else c = a - d;
    if (c > b) c = b;
    if (c < 0) c = 0;
    document.getElementById("config_main").scrollTop = c;
}

function select_背景(num) {
    var a = 0;
    var b = document.getElementsByClassName("ステージ選択");
    var c = b.length;
    while (a < c) {
        document.getElementsByClassName("ステージ選択")[a].style.backgroundColor = "white";
        a++;
    }
    document.getElementsByClassName("ステージ選択")[num - 1].style.backgroundColor = "lightblue";
}

function 編成_選択2(str) {
    var base_num = 0; //押したタイミングで選択されている場所
    var after_num = 0; //押したあとの座標
    var a = document.getElementsByClassName("編成_コントロール");
    var b = 0;
    while (b < a.length) {
        if (a[b].style.backgroundColor == "lightpink") break;
        b++;
    }
    //base_numが
    //0～11ならどこかが選択されている状態
    //12ならどこも選択されていない状態
    base_num = b;
    after_num = base_num;
    switch (str) {
        case "上":
            switch (base_num) {
                case 0:
                case 1:
                case 2:
                    after_num += 6;
                    break;

                case 9:
                case 10:
                case 11:
                    break;

                default:
                    after_num -= 3;
                    break;
            }
            break;

        case "下":
            switch (base_num) {
                case 6:
                case 7:
                case 8:
                    after_num -= 6;
                    break;

                case 9:
                case 10:
                case 11:
                    break;

                default:
                    after_num += 3;
                    break;
            }
            break;

        case "左":
            switch (base_num) {
                case 0:
                    after_num = 11;
                    break;

                case 3:
                case 6:
                    after_num += 2;
                    break;

                case 9:
                    after_num = 2;
                    break;

                default:
                    after_num--;
                    break;
            }
            break;

        case "右":
            switch (base_num) {
                case 2:
                    after_num = 9;
                    break;

                case 5:
                case 8:
                    after_num -= 2;
                    break;

                case 11:
                    after_num = 0;
                    break;

                default:
                    after_num++;
                    break;
            }
            break;

        default:
            break;
    }
    if (b == 12) after_num = 0;
    b = 0;
    while (b < a.length) {
        a[b].style.backgroundColor = "#FFFFFFD0";
        b++;
    }
    if (after_num < 9) ステータス値表示(after_num + 1);
    else ステータス値表示(0);
    document.getElementsByClassName("編成_コントロール")[after_num].style.backgroundColor = "lightpink";
}

function Aボタン() {
    switch (現在画面) {
        case "title":
            switch (Commands) {
                case "LRLRLLRR":
                    BGM.pause();
                    SE.src = "bgm/battle/lv_up.mp3";
                    SE.play();
                    user = false;
                    if (難易度 == "HARD") 難易度 = "NORMAL";
                    else 難易度 = "HARD";
                    setTimeout(function() {
                        bgm(現在画面);
                        user = true;
                        起動ロード();
                    }, 2500);
                    Commands = "";
                    break;

                case "uldr":
                    BGM.pause();
                    SE.src = "bgm/battle/lv_up.mp3";
                    SE.play();
                    user = false;
                    if (難易度 == "EASY") 難易度 = "NORMAL";
                    else 難易度 = "EASY";
                    setTimeout(function() {
                        bgm(現在画面);
                        user = true;
                        起動ロード();
                    }, 2500);
                    Commands = "";
                    break;

                case "LRLRXXYY":
                    BGM.pause();
                    SE.src = "bgm/battle/lv_up.mp3";
                    SE.play();
                    user = false;
                    if (難易度 == "LUNATIC") 難易度 = "NORMAL";
                    else 難易度 = "LUNATIC";
                    setTimeout(function() {
                        bgm(現在画面);
                        user = true;
                        起動ロード();
                    }, 2500);
                    Commands = "";
                    break;

                case "uuddlrlrB":
                    BGM.pause();
                    SE.src = "bgm/battle/lv_up.mp3";
                    SE.play();
                    user = false;
                    if (難易度 == "EXTRA") 難易度 = "NORMAL";
                    else 難易度 = "EXTRA";
                    setTimeout(function() {
                        bgm(現在画面);
                        user = true;
                        起動ロード();
                    }, 2500);
                    Commands = "";
                    break;

                default:
                    if (user == true) {
                        user = false;
                        document.getElementsByClassName("ステージ選択")[0].style.backgroundColor = "lightblue";
                        ステージ選択_コントローラー選択_num += 2;
                        起動ロード();
                        Commands = "";
                    }
                    break;
            }
            break;

        case "select":
            var a = document.getElementsByClassName("ステージ選択");
            var b = 0;
            var c = false;
            while (b < a.length) {
                if (a[b].style.backgroundColor == "lightblue") c = true;
                b++;
            }
            if (c) {
                if (user == true && ステージ選択_コントローラー選択_num >= 0) {
                    user = false;
                    編成画面へ(ステージ選択_コントローラー選択_num);
                } else if (user == true && ステージ選択_コントローラー選択_num == -1) {
                    user = false;
                    ステージ選択_コントローラー選択_num += 2;
                    編成画面へ(0);
                }
            }
            break;

        case "config":
            var a = document.getElementById("master");
            if (a.value < 100) {
                var b = Math.floor(Number(a.value) / 10) * 10;
                document.title = b;
                a.value = (b + 10);
            }
            break;

        case "編成":
            var a = document.getElementsByClassName("編成_コントロール");
            var b = 0;
            while (b < a.length) {
                if (a[b].style.backgroundColor == "lightpink") break;
                b++;
            }
            if (b == 12) {
                if (user == true) {
                    var a = document.getElementsByClassName("編成_コントロール");
                    var b = 0;
                    while (b < a.length) {
                        a[b].style.backgroundColor = "#FFFFFFD0";
                        b++;
                    }
                    user = false;
                    戦闘画面へ();
                }
            } else a[b].click();
            break;

        case "battle":
            攻撃(1);
            break;

        case "victory":
            document.getElementsByClassName("勝利_ボタン")[0].click();
            break;

        case "確認画面":
            確認画面_選択(true);
            break;

        default:
            break;
    }
}

function Bボタン() {
    switch (現在画面) {
        case "title":
            Commands += "B";
            break;

        case "select":
            selectボタン();
            break;

        case "config":
            btm_ステージ();
            break;

        case "編成":
            if (user == true) {
                user = false;
                se('戻る');
                btm_ステージ();
            }
            break;

        case "battle":
            攻撃(2);
            break;

        case "確認画面":
            確認画面_選択(false);
            break;

        default:
            break;
    }
}

var ミュート_cnt = 0;

function Xボタン() {
    switch (現在画面) {
        case "title":
            Commands += "X";
            break;

        case "config":
            ミュート_cnt++;
            document.getElementById("mute").value = (ミュート_cnt % 2);
            break;

        case "編成":
            document.getElementById("skip").click();
            break;

        case "battle":
            攻撃(3);
            break;

        default:
            break;
    }
}

function Yボタン() {
    switch (現在画面) {
        case "title":
            Commands += "Y";
            break;

        case "config":
            var a = document.getElementById("master");
            if (a.value > 0) {
                var b = Math.floor(Number(a.value) / 10) * 10;
                var c = Math.floor(Number(a.value) % 10);
                document.title = b;
                if (c == 0) a.value = (b - 10);
                else a.value = (Number(a.value) - c);
            }
            break;

        case "battle":
            攻撃(4);
            break;

        default:
            break;
    }
}

function 上ボタン() {
    switch (現在画面) {
        case "title":
            Commands += "u";
            break;

        case "select":
            ステージ選択_コントローラー選択_num--;
            if (ステージ選択_コントローラー選択_num < 1) ステージ選択_コントローラー選択_num = 5;
            ステージ選択_コントローラー選択(ステージ選択_コントローラー選択_num);
            select_背景(ステージ選択_コントローラー選択_num);
            break;

        case "編成":
            編成_選択2("上");
            break;

        case "battle":
            敵選択(1);
            break;

        case "config":
            LS上();
            break;

        default:
            break;
    }
}

function 下ボタン() {
    switch (現在画面) {
        case "title":
            Commands += "d";
            break;

        case "select":
            if (ステージ選択_コントローラー選択_num == -1) ステージ選択_コントローラー選択_num = 1;
            else ステージ選択_コントローラー選択_num++;
            if (ステージ選択_コントローラー選択_num > 5) ステージ選択_コントローラー選択_num = 1;
            ステージ選択_コントローラー選択(ステージ選択_コントローラー選択_num);
            select_背景(ステージ選択_コントローラー選択_num);
            break;

        case "編成":
            編成_選択2("下");
            break;

        case "battle":
            敵選択(1);
            break;

        case "config":
            LS下();
            break;

        default:
            break;
    }
}

function 右ボタン() {
    switch (現在画面) {
        case "title":
            Commands += "r";
            break;

        case "編成":
            編成_選択2("右");
            break;

        case "config":
            var a = document.getElementById("master");
            if (a.value <= 100) {
                a.value = Number(a.value) + 1;
            }
            break;

        case "battle":
            敵選択(2);
            break;

        default:
            break;
    }
}

function 左ボタン() {
    switch (現在画面) {
        case "title":
            Commands += "l";
            break;

        case "編成":
            編成_選択2("左");
            break;

        case "config":
            var a = document.getElementById("master");
            if (a.value >= 0) {
                a.value = Number(a.value) - 1;
            }
            break;

        case "battle":
            敵選択(0);
            break;

        default:
            break;
    }
}

function selectボタン() {
    switch (現在画面) {
        case "title":
            Commands = "";
            break;

        case "select":
            btm_タイトルに戻る();
            break;

        case "config":
            btm_タイトルに戻る();
            break;

        case "編成":
            btm_ステージ();
            break;

        case "battle":
            if (user == true) {
                逃げる();
            }
            break;

        default:
            break;
    }
}

function startボタン() {
    switch (現在画面) {
        case "title":
            bgm("title");
            break;

        case "select":
            Aボタン();
            break;

        case "config":
            document.getElementById("se_test").click();
            break;

        case "編成":
            if (user == true) {
                var a = document.getElementsByClassName("編成_コントロール");
                var b = 0;
                while (b < a.length) {
                    a[b].style.backgroundColor = "#FFFFFFD0";
                    b++;
                }
                user = false;
                戦闘画面へ();
            }
            break;

        case "確認画面":
            Aボタン();
            break;

        default:
            break;
    }
}

function Lボタン() {
    switch (現在画面) {
        case "title":
            Commands += "L";
            break;

        case "select":
            if (user == true) {
                user = false;
                btm_コンフィグ();
            }
            break;

        case "編成":
            カウント2('-');
            break;

        case "config":
            if (user == true) {
                user = false;
                btm_ステージ();
            }
            break;

        default:
            break;
    }
}

function Rボタン() {
    switch (現在画面) {
        case "title":
            Commands += "R";
            break;

        case "select":
            if (user == true) {
                user = false;
                btm_コンフィグ();
            }
            break;

        case "編成":
            カウント2('+');
            break;

        case "config":
            if (user == true) {
                user = false;
                btm_ステージ();
            }
            break;

        default:
            break;
    }
}

function ZLボタン() {
    switch (現在画面) {
        case "title":
            Commands += "ZL";
            break;

        case "config":
            document.getElementById("jpn").click();
            break;

        case "編成":
            カウント2('-10');
            break;

        default:
            break;
    }
}

function ZRボタン() {
    switch (現在画面) {
        case "title":
            Commands += "ZR";
            break;

        case "config":
            document.getElementById("eng").click();
            break;

        case "編成":
            カウント2('+10');
            break;

        default:
            break;
    }
}

function LS上() {
    switch (現在画面) {
        case "select":
            上ボタン();
            break;

        case "config":
            config_コントローラー選択(-1);
            break;

        default:
            break;
    }
}

function LS下() {
    switch (現在画面) {
        case "select":
            下ボタン();
            break;

        case "config":
            config_コントローラー選択(1);
            break;

        default:
            break;
    }
}

function LS右() {
    switch (現在画面) {
        case "config":
            var a = document.getElementById("volume");
            if (a.value <= 100) {
                a.value = Number(a.value) + 1;
            }
            break;

        default:
            break;
    }
}

function LS左() {
    switch (現在画面) {
        case "config":
            var a = document.getElementById("volume");
            if (a.value >= 0) {
                a.value = Number(a.value) - 1;
            }
            break;

        default:
            break;
    }
}

function RS上() {
    switch (現在画面) {
        case "config":
            break;

        default:
            break;
    }
}

function RS下() {
    switch (現在画面) {
        case "config":
            break;

        default:
            break;
    }
}

function RS右() {
    switch (現在画面) {
        case "config":
            var a = document.getElementById("se");
            if (a.value <= 100) {
                a.value = Number(a.value) + 1;
            }
            break;

        default:
            break;
    }
}

function RS左() {
    switch (現在画面) {
        case "config":
            var a = document.getElementById("se");
            if (a.value >= 0) {
                a.value = Number(a.value) - 1;
            }
            break;

        default:
            break;
    }
}