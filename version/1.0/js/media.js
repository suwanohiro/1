class media {
    static method = [];

    static Player_Add() {
        this.method[this.method.length] = new Audio();
    }

    static Player_length() {
        return this.method.length;
    }

    static Play(num, url, vol, loop, memo) {
        if (num > this.method.length) this.Error("Play");
        else {
            if (num == this.method.length) this.Player_Add();
            if (vol == undefined) vol = 100;
            if (loop == undefined) loop = false;
            if (memo == undefined) memo = "";
            this.src(num, url);
            this.loop(num, loop);
            this.volume(num, vol);
            this.method[num].play();
            this.method[num].value = memo;
        }
    }

    static Pause(num) {
        if (num >= this.method.length) this.Error("Pause");
        else this.method[num].pause();
    }

    static volume(num, vol) {
        vol = Math.abs(vol);
        if (vol > 100) vol = 100;
        this.method[num].volume = vol / 100;
    }

    static loop(num, bool) {
        this.method[num].loop = bool;
    }

    static src(num, url) {
        if (num >= this.method.length) this.Error("src");
        else this.method[num].src = url;
    }

    static Get_volume(num) {
        if (num >= this.method.length) this.Error("Get_volume");
        else return this.method[num].volume;
    }

    static Get_loop(num) {
        if (num >= this.method.length) this.Error("Get_loop");
        else return this.method[num].loop;
    }

    static Get_src(num) {
        if (num >= this.method.length) this.Error("Get_src");
        else return this.method[num].src;
    }

    static Now_time(num) {
        if (num >= this.method.length) this.Error("Now_time");
        else return this.method[num].currentTime;
    }

    static All_time(num) {
        if (num >= this.method.length) this.Error("All_time");
        else return this.method[num].duration;
    }

    static Read_memo(num) {
        if (num >= this.method.length) this.Error("Read_memo");
        else return this.method[num].value;
    }

    static Search_memo(str) {
        var data = this.method;
        var memo_data = [];
        for (var a = 0; a < data.length; a++) {
            memo_data[a] = data[a].value;
        }
        return memo_data.indexOf(str);
    }

    static Error(str) {
        console.log(`method.${str} : not length`);
    }
}