class music {
    static Music_Library = new Array(0);
    static Structure = {
        Scene: new Array(0),
        DisplayID: new Array(0),
        FolderName: new Array(0),
        FileName: new Array(0),
        FileType: new Array(0),
        MusicName: new Array(0),
        FileWebLink: new Array(0)
    };

    static onload() {
        this.Set_MusicLibrary();
        this.Get_FileLocalLink("タイトル");
    }

    static Set_MusicLibrary() {
        var csv = text.csv_read("DataBase/MusicLibrary.csv");
        this.Music_Library = csv;
        this.Set_Structure(csv);
    }

    static Get_MusicLibrary() {
        return this.Music_Library;
    }

    static Set_Structure(data) {
        var Scene = new Array(0);
        var DisplayID = new Array(0);
        var FolderName = new Array(0);
        var FileName = new Array(0);
        var FileType = new Array(0);
        var MusicName = new Array(0);
        var FileWebLink = new Array(0);
        for (var a = 1; a < data.length; a++) {
            var b = a - 1;
            //シーン
            Scene[b] = data[a][0];

            //画面ID
            DisplayID[b] = data[a][1];

            //フォルダ名
            FolderName[b] = data[a][2];

            //ファイル名
            FileName[b] = data[a][3];

            //ファイル形式
            FileType[b] = data[a][4];

            //曲名
            MusicName[b] = data[a][5];

            //リンク
            FileWebLink[b] = data[a][6];
        }

        this.Structure.Scene = Scene;
        this.Structure.DisplayID = DisplayID;
        this.Structure.FolderName = FolderName;
        this.Structure.FileName = FileName;
        this.Structure.FileType = FileType;
        this.Structure.MusicName = MusicName;
        this.Structure.FileWebLink = FileWebLink;

        log.info(this.name, this.Set_Structure.name, `楽曲情報セット完了`);
    }

    static Get_Structure() {
        return this.Structure;
    }

    static Get_FileLocalLink(SceneName) {
        var data = this.Get_Structure();
        var FileName = data.FileName;
        var FileType = data.FileType;
        var FolderName = data.FolderName;
        var Scene = data.Scene;
        var result;
        var Search = Scene.indexOf(SceneName);
        if (Search >= 0) {
            //asd
        } else {
            log.error_end(this.name, this.Get_FileLocalLink.name, "合致するシーンが見つかりませんでした。");
        }
        log.info(this.name, this.Get_FileLocalLink.name, `${Search}`);
    }
}