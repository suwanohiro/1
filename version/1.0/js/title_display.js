class title_display {
    static onload() {
        this.addEvent();
        this.Page_title();
        this.title();
        this.Version();
    }

    static addEvent() {
        var title_click = document.getElementById("title_click")

        title_click.addEventListener("click", function () {
            //タイトル画面クリック時の動作
        });
    }

    static Page_title() {
        //タイトル画面時のページタイトルを設定
        var GameTitle = base_data.Get_GameTitle();
        var GameVersion = `Ver: ${base_data.Get_GameVersion()}`;

        document.title = `${GameTitle} ( ${GameVersion} )`;
    }

    static title() {
        //ゲームタイトルを表示
        var titles = base_data.Get_GameTitle();
        var game_title = document.getElementsByClassName("game_title");
        game_title[0].innerHTML = titles;
    }

    static Version() {
        //ゲームバージョンを表示
        var ver = base_data.Get_GameVersion();
        var Game_Version = document.getElementById("Game_Version");
        Game_Version.innerHTML = `Version: ${ver}`;
    }
}