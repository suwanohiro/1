class log {
    static Get_Console_Color() {
        const result = {
            black: "\u001b[30m",
            red: "\u001b[31m",
            green: "\u001b[32m",
            yellow: "\u001b[33m",
            blue: "\u001b[34m",
            magenta: "\u001b[35m",
            cyan: "\u001b[36m",
            white: "\u001b[37m",
            reset: "\u001b[0m"
        };
        return result;
    }

    static Get_Date() {
        var date = new Date;
        var Year = date.getFullYear();
        var Month = this.FillZero((date.getMonth() + 1), 2);
        var Day = this.FillZero((date.getDate()), 2);
        var Hour = this.FillZero((date.getHours()), 2);
        var Minutes = this.FillZero((date.getMinutes()), 2);
        var Second = this.FillZero((date.getSeconds()), 2);
        var Msecond = this.FillZero((date.getMilliseconds()), 3);
        return `[${Year}/${Month}/${Day} ${Hour}:${Minutes}:${Second}.${Msecond}]`;
    }

    static error(Class, Function, Str) {
        var str = this.Sentence("error", Class, Function, Str);
        console.log(str);
    }

    static error_end(Class, Function, Str) {
        var str = this.Sentence("error", Class, Function, Str);
        console.log(`${str}\n処理を中断します。`);
    }

    static FillZero(num1, num2) {
        var str = "";
        for (var a = 0; a < num2; a++) {
            str += "0";
        }
        return (str + num1).slice(num2 * -1);
    }

    static info(Class, Function, Str) {
        var str = this.Sentence("info", Class, Function, Str);
        console.log(str);
    }

    static Sentence(Type, Class, Function, Str) {
        //ログの文章作成
        var color = this.Get_Console_Color();
        var location = "";
        var now_date = this.Get_Date();

        //InfoかErrorかOtherか
        switch (Type) {
            case "Error":
            case "error":
                Type = `${color.red}[Error]${color.reset}`;
                break;

            case "Info":
            case "info":
                Type = `${color.green}[Info ]${color.reset}`;
                break;

            default:
                Type = `${color.magenta}[Other]${color.reset}`;
                break;
        }

        //Classが指定されているかどうか
        if (Str != undefined) {
            //Classが指定されている場合
            var cs = color.green + Class + color.reset;
            var point = color.white + "." + color.reset;
            var fc = color.yellow + Function + color.reset;
            var parentheses = color.blue + "()" + color.reset;
            location = cs + point + fc + parentheses;
        } else {
            //Classが指定されていない場合
            Str = Function;
            Function = Class;
            location = color.yellow + Function + color.reset + color.blue + "()" + color.reset;
        }
        return `${Type} ${color.cyan}${now_date} [Location: ${color.reset}${location}${color.reset}${color.cyan}]${color.reset}\n\t${Str}`;
    }
}