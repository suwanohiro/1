window.onload = function () {
    base_data.onload();
    addEvent.onload();
    music.onload();
    Display_move.onload();
    title_display.onload();
}

class base_data {
    //ゲームの基本情報について
    static Game_Title = "ゲームタイトル";
    static Game_Version = "0.2022.09.10α";
    static Initial_Screen = {
        name: "タイトル",
        id: "test"
    };
    static Last_Update = "2022/09/10";
    static SceneToDisplayID = {
        name: new Array(0),
        id: new Array(0)
    };

    static onload() {
        this.Set_SceneToDisplayID();
        this.Set_InitialScreenID();
    }

    static Set_SceneToDisplayID() {
        var name = new Array(0);
        var id = new Array(0);
        var data = text.csv_read("DataBase/SceneToDisplayID.csv");
        for (var a = 1; a < data.length; a++) {
            var b = a - 1;

            //シーン名
            name[b] = data[a][0];

            //画面ID
            id[b] = data[a][1];
        }

        this.SceneToDisplayID.name = name;
        this.SceneToDisplayID.id = id;
    }

    static Get_SceneToDisplayID() {
        return this.SceneToDisplayID;
    }

    static Get_GameVersion() {
        return this.Game_Version;
    }


    static Get_GameTitle() {
        return this.Game_Title;
        return this.Game_Version;
    }

    static Set_InitialScreenID() {
        var data = this.Get_SceneToDisplayID();
        var input = this.Get_InitialScreen().name;
        var work = data.name.indexOf(input);

        this.Initial_Screen.id = data.id[work];
    }

    static Get_InitialScreen() {
        return this.Initial_Screen;
    }
}

class addEvent {
    static onload() {
        this.register();
    }

    //イベント処理を追加
    static register() {
        var title_screen = document.getElementById("title_screen");

        title_screen.addEventListener("click", function (e) {
        });
    }
}

class Display_move {
    static Initial_Screen = base_data.Get_InitialScreen().id;
    static now_display = this.Initial_Screen;
    static id_data = new Array(0);
    static Screen_Movement_History = new Array(0);

    static onload() {
        this.register();
        this.Move(this.now_display, this.now_display);
    }

    static Set_NowDisplay(str) {
        this.now_display = String(str);
    }

    static Get_NowDisplay() {
        return this.now_display;
    }

    static register() {
        //[main_display]のIDを[id_data]配列に格納
        var main_display = document.getElementsByClassName("main_display");
        var local_data = new Array(main_display.length);
        this.id_data = new Array(local_data.length);

        for (var a = 0; a < local_data.length; a++) {
            local_data[a] = main_display[a].id;
        }

        this.id_data = local_data;
    }

    static Hidden() {
        //全画面を非表示に
        var IdData = this.id_data;
        for (var a = 0; a < IdData.length; a++) {
            var element = document.getElementById(IdData[a]);
            element.style.display = "none";
        }
    }


    static Move(Source, Destination) {
        //画面移動処理

        var IdData = this.id_data;

        //移動元、移動先が指定されていなかった場合はコンソールにエラー文を表示して処理終了
        if (Source == undefined || Destination == undefined) {
            log.error(this.name, this.Move.name, "画面移動先が指定されていません。");
            return undefined;
        }

        //存在しないIDが指定されたらコンソールにエラー文を表示して処理終了
        if (IdData.indexOf(Source) == -1 || IdData.indexOf(Destination) == -1) {
            log.error(this.name, this.Move.name, `存在しないIDが指定されました。\n\t移動元ID [Source]\t\t : ${Source}\n\t移動先ID [Destination]\t : ${Destination}`);
            return undefined;
        }

        if (Source != null) {
            //画面移動履歴を配列に格納
            //一番最初の時(nullの時)は実行しない
            var History = this.Screen_Movement_History;
            this.Screen_Movement_History = new Array(History.length + 1);
            History[History.length] = Source;
            this.Screen_Movement_History = History;
        }

        //全画面非表示に
        this.Hidden();

        //移動先画面を表示
        var element = document.getElementById(Destination);
        element.style.display = "block";

        log.info(this.name, this.Move.name, `[${Source}] から [${Destination}] へ画面移動完了`);
    }
}
