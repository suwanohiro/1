var a = 0;

window.onload = function () {
    log.info("onload", "非同期処理テスト");

    setTimeout(function () {
        a = 1 + 1;
    }, 3000);

    console.log(a);

    setTimeout(function () {
        console.log(a);
    }, 4000);
}