# ソシャゲ 1

## 概要

&emsp;2021 年度に制作した[ソシャゲ](http://suwanohiro.html.xdomain.jp/school/kadai/game.html)を今自分が持っている技術でもう一度作ってみようということで制作します。

<br><br>

# 各種進捗 (最終更新：2022/09/07)

## ![Progress](https://progress-bar.dev/0/?title=00%20/%2002)：ゲーム根幹設定等
<br>

>![Progress](https://progress-bar.dev/50/?title=01%20/%2002)：実装中画面(未完成含)<br>
>-
>- [x] タイトル画面
>- [ ] 戦闘画面

>![Progress](https://progress-bar.dev/0/?title=00%20/%2002)：完成済画面
>-
>- [ ] タイトル画面
>- [ ] 戦闘画面

<br><br>

## ![Progress](https://progress-bar.dev/25/?title=01%20/%2004)：タイトル画面
<br>

>![Progress](https://progress-bar.dev/66/?title=02%20/%2003)：画面構成 (HTML)
>-
>- [x] ゲームタイトル表示
>- [x] ゲームバージョン表示
>- [ ] セーブデータ引き継ぎ・削除ボタン

>![Progress](https://progress-bar.dev/16/?title=01%20/%2006)：画面装飾 (CSS)
>-
>- [ ] 各種ボタンデザイン
>- [x] フォント設定
>- [ ] 要素配置設定
>- [ ] 配色設定
>- [ ] 背景画像選定
>- [ ] 背景画像配置

>![Progress](https://progress-bar.dev/0/?title=00%20/%2005)：動作処理 (Javascript)
>-
>- [ ] 画面移動処理
>- [ ] BGM再生処理
>- [ ] セーブデータ読み込み処理
>- [ ] セーブデータ引き継ぎ処理
>- [ ] セーブデータ削除処理

>![Progress](https://progress-bar.dev/100/?title=01%20/%2001)：BGM関連
>-
>> ![Progress](https://progress-bar.dev/100/?title=05%20/%2005)：タイトル画面BGM
>>- [x] 選定
>>- [x] 変換
>>- [x] 圧縮
>>- [x] 配置
>>- [x] BGMリストへ追加




























